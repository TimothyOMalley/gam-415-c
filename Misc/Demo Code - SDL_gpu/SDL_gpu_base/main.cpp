#include <iostream>
#include "SDL_gpu.h"
#include "NFont_gpu.h"
#include "Button.h"
using namespace std;

int main(int argc, char* argv[]){

	GPU_Target* mainScreen = GPU_Init(800, 600, GPU_DEFAULT_INIT_FLAGS);
	//SDL_WINDOW_POPUP_MENU

	SDL_SetWindowTitle(SDL_GetWindowFromID(mainScreen->context->windowID), "Editor");

	//GPU_Image* smileImage = GPU_LoadImage("smile.png");
	//if (smileImage == nullptr)
	//	return 2;

	NFont font;
	font.load("FreeSans.ttf", 14);

	const Uint8* keystates = SDL_GetKeyboardState(nullptr);

	int mx = 0, my = 0;

	SDL_Event event;
	bool done = false;

	while (!done)
	{

		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				done = true;
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
					done = true;
				if (event.key.keysym.sym == SDLK_SPACE) {
				}
				if (event.key.keysym.sym == SDLK_z) { //switch draw type
				}
				if (event.key.keysym.sym == SDLK_w || event.key.keysym.sym == SDLK_UP) { //pan up
				}
				if (event.key.keysym.sym == SDLK_a || event.key.keysym.sym == SDLK_LEFT) { //pan left
				}
				if (event.key.keysym.sym == SDLK_s || event.key.keysym.sym == SDLK_DOWN) { //pan down
				}
				if (event.key.keysym.sym == SDLK_d || event.key.keysym.sym == SDLK_RIGHT) { // pan right
				}
				if (event.key.keysym.sym == SDLK_q) { //zoom out
				}
				if (event.key.keysym.sym == SDLK_e) { // zoom in 
				}
				if (event.key.keysym.sym == SDLK_x) { //reset camera
				}
			}
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				if (event.button.button == SDL_BUTTON_LEFT) {
				}
			}
		}

		SDL_GetMouseState(&mx, &my);

		if (keystates[SDL_SCANCODE_A] || keystates[SDL_SCANCODE_LEFT]) {}
		else if (keystates[SDL_SCANCODE_D] || keystates[SDL_SCANCODE_RIGHT]) {}
		if (keystates[SDL_SCANCODE_W] || keystates[SDL_SCANCODE_UP]) {}
		else if (keystates[SDL_SCANCODE_S] || keystates[SDL_SCANCODE_DOWN]) {
		}

		//GPU_Blit(smileImage, nullptr, screen, x, y);
		GPU_ClearRGB(mainScreen, 255, 255, 255);
		GPU_SetLineThickness(3.0f);
		GPU_Line(mainScreen, 25, 575, 775, 575, GPU_MakeColor(255, 0, 0, 255));
		GPU_Line(mainScreen, 25, 25, 25, 575, GPU_MakeColor(0, 255, 0, 255));

		GPU_Pixel(mainScreen, 0, 0, GPU_MakeColor(0, 0, 255, 255));
		font.draw(mainScreen, mainScreen->w - 50, 10, NFont::AlignEnum::RIGHT, "Mouse: (%d, %d)", mx, my);

		GPU_Flip(mainScreen);

		SDL_Delay(1);
	}
	//GPU_FreeImage(smileImage);
	font.free();

	GPU_Quit();
	
	return 0;
}