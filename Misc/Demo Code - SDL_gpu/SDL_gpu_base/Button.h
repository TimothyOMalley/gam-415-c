#pragma once
#include "SDL_gpu.h"
#include "NFont_gpu.h"
#include <string>

class Button {

private:
	string* button_text;

public:
	Button();

	Button(string buttonText);

	~Button();

	void DrawButton(GPU_Target screen, NFont font, int x, int y, int h, int w);

	string* GetButtonText();

	void SetButtonText(string* buttonText);

};

#include "Button.cpp"