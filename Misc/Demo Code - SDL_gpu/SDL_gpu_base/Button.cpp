#include "Button.h"

using namespace std;

class Button{

	string* button_text = new string();

	Button() {
		*button_text = "";
	}

	Button(string buttonText) {
		*button_text = buttonText;
	}

	~Button() {
		delete button_text;
	}
	
	string* GetButtonText() {
		return button_text;
	}

	void DrawButton(GPU_Target screen, NFont font, int x, int y, int h, int w) {
	}

	void SetButtonText(string* buttonText) {
		button_text = buttonText;
	}
};