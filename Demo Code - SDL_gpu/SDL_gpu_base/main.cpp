#include <iostream>
#include "SDL_gpu.h"
#include "NFont_gpu.h"
#include <iostream>
#include <fstream>
using namespace std;

//Equations from easings.net that needed their own function
float GetEaseOutBounce(float x) {
	const float n1 = 7.5625;
	const float d1 = 2.75;
	if (x < 1 / d1)
		return n1 * x * x;
	if (x < 2 / d1)
		return n1 * (x -= 1.5 / d1) * x + 0.75;
	if (x < 2.5 / d1)
		return n1 * (x -= 2.25 / d1) * x + 0.9375;
	return n1 * (x -= 2.625 / d1) * x + 0.984375;
}

int main(int argc, char* argv[])
{


	GPU_Target* screen = GPU_Init(800, 600, GPU_DEFAULT_INIT_FLAGS);
	if (screen == nullptr)
		return 1;

	SDL_SetWindowTitle(SDL_GetWindowFromID(screen->context->windowID), "My Awesome Game");
	
	GPU_Image* smileImage = GPU_LoadImage("smile.png");
	if (smileImage == nullptr)
		return 2;

	NFont font;
	font.load("FreeSans.ttf", 14);

	const Uint8* keystates = SDL_GetKeyboardState(nullptr);

	float x = 0;
	float y = 0;
	float panZoomSpeed = 5; //speed at which panning or zooming happens per frame (zooming is 1/100 of pan zoom speed)
	int cycle = 0;

	string interpType = "Ease in Sine"; //What is written at the top of the screen

	enum DrawType { picture, plot };
	DrawType drawType = picture; //decides how to draw the graph

	int mx = 0, my = 0;

	SDL_Event event;
	bool done = false;

	ofstream file;
	file.open("MousePosition.txt");

	while (!done)
	{

		string interpolation[30] = {  //Name of interpolation
			"Ease in Sine", "Ease out Sine", "Ease in out Sine", // done
			"Ease in Quad", "Ease out Quad", "Ease in out Quad", // done
			"Ease in Cubic", "Ease out Cubic", "Ease in out Cubic", // done
			"Ease in Quart", "Ease out Quart", "Ease in out Quart",
			"Ease in Quint", "Ease out Quint", "Ease in out Quint",
			"Ease in Expo", "Ease out Expo", "Ease in Out Expo",
			"Ease in Circ", "Ease out Circ", "Ease in out Circ",
			"Ease in Back", "Ease out Back", "Ease in out Back",
			"Ease in Elastic", "Ease out Elastic", "Ease in out Elastic",
			"Ease in Bounce", "Ease out Bounce", "Ease in out Bounce"
		};

		const float c1 = 1.70158;
		const float c2 = c1 * 1.525;
		const float c3 = c1 + 1;
		const float c4 = (2 * M_PI) / 3;
		const float c5 = (2 * M_PI) / 4.5;

		float equations[30] = { //Equation coresponding with name of interpolation (all from easings.net)
			1 - cos((x / 800 * M_PI) / 2), //Ease in Sine
			sin((x / 800 * M_PI) / 2), //Ease out Sine
			-(cos(M_PI * x / 800) - 1) / 2, //Ease in out Sine

			(x / 800) * (x / 800), //Ease in quad
			1-(1-(x / 800))*(1-(x / 800)), //Ease out Quad
			(x / 800) < 0.5 ? 2 * (x / 800) * (x / 800) : 1 - pow(-2 * (x / 800) + 2, 2) / 2, //Ease in out Quad

			(x / 800) * (x / 800) * (x / 800), //ease in cubic
			1 - pow(1 - (x / 800), 3), //ease out cubic
			(x / 800) < 0.5 ? 4 * (x / 800) * (x / 800) * (x / 800) : 1 - pow(-2 * (x / 800) + 2, 3) / 2, //Ease in out cubic

			(x / 800) * (x / 800) * (x / 800) * (x / 800), //Ease in Quart
			1 - pow(1 - (x / 800), 4), //Ease out Quart
			(x / 800) < 0.5 ? 8 * (x / 800) * (x / 800) * (x / 800) * (x / 800) : 1 - pow(-2 * (x / 800) + 2, 4) / 2, //Ease in out Quart

			(x / 800) * (x / 800) * (x / 800) * (x / 800) * (x / 800), //Ease in Quint
			1 - pow(1 - (x / 800), 5), //Ease out Quint
			(x / 800) < 0.5 ? 16 * (x / 800) * (x / 800) * (x / 800) * (x / 800) * (x / 800) : 1 - pow(-2 * (x / 800) + 2, 5) / 2, //Ease in out Quint

			(x / 800) == 0 ? 0 : pow(2, 10 * (x / 800) - 10), //Ease in Expo
			(x / 800) == 1 ? 1 : 1 - pow(2, -10 * (x / 800)), //Ease out Expo
			(x / 800) == 0 ? 0 : (x / 800) == 1 ? 1 : (x / 800) < 0.5 ? pow(2, 20 * (x / 800) - 10) / 2 : (2 - pow(2, -20 *(x / 800) + 10)) / 2, //Ease in out Expo

			1 - sqrt(1 - pow((x / 800), 2)), //Ease in Circ
			sqrt(1 - pow((x / 800) - 1, 2)), //Ease out Circ
			(x / 800) < 0.5 ? (1-sqrt(1-pow(2 * (x / 800), 2))) / 2 : (sqrt(1 - pow(-2 * (x / 800) + 2, 2)) + 1) /2, //Ease in out Circ

			c3 * (x / 800) * (x / 800) * (x / 800) - c1 * (x / 800) * (x / 800), //Ease in Back
			1 + c3 * pow((x / 800) - 1, 3) + c1 * pow((x / 800) - 1, 2), //Ease out Back
			(x / 800) < 0.5 ? (pow(2 * (x / 800), 2) * ((c2 + 1) * 2 * (x / 800) - c2)) / 2 : (pow(2 * (x / 800) - 2, 2) * ((c2 + 1) * ((x / 800) * 2 - 2) + c2) + 2) / 2, //Ease in out Back

			(x / 800) == 0 ? 0 : (x / 800) == 1 ? 1 : -pow(2, 10 * (x / 800) - 10) * sin(((x / 800) * 10 - 10.75) * c4), //Ease in Elastic
			(x / 800) == 0 ? 0 : (x / 800) == 1 ? 1 : pow(2, -10 * (x / 800)) * sin(((x / 800) * 10 - 0.75) * c4) + 1, //Ease out Elastic
			(x / 800) == 0 ? 0 : (x / 800) == 1 ? 1 : (x / 800) <0.5 ? -(pow(2, 20 * (x / 800) - 10) * sin((20 * (x / 800) - 11.125) * c5)) / 2 : (pow(2, -20 * x + 10) * sin((20 * x - 11.125) * c5)) / 2 + 1, //Ease in out Elastic

			1 - GetEaseOutBounce( 1 - (x / 800) ), //Ease in Bounce
			GetEaseOutBounce(x / 800), //Ease Out Bounce
			(x / 800) < 0.5 ? (1 - GetEaseOutBounce(1 - 2 * (x / 800))) / 2 : (1 + GetEaseOutBounce(2 * (x / 800) - 1)) / 2 //Ease In Out Bounce
            if(x<0.5){
                1-GetEaseOutBounce(1-2*x)/2
            }else{
                1+GetEaseOutBounce(2*x-1)/2
            }
		};

		float t = SDL_GetTicks() / 1000.0f; //Time variable
		while (t > 1.0f)
			t -= 1; //time variable reset

		x = (t*750)+25; //x stays linear; *750 and +25 to give window a 25 pixel border on either side left and right
		y = (equations[cycle]*550)+25; //y of x; *550 and +25 to give the window a 25 pixel border on either side top and bottom


		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				done = true;
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
					done = true;
				if (event.key.keysym.sym == SDLK_SPACE){
					cycle++; //increase the cycle 
					if (cycle > 29) { cycle = 0; } //if the cycle passes cieling, reset
					interpType = interpolation[cycle]; //reset string at top
				}
				if (event.key.keysym.sym == SDLK_z) { //switch draw type
					switch (drawType) {
					case picture:
						drawType = plot;
						break;
					case plot:
						drawType = picture;
						break;
					}
				}
				if (event.key.keysym.sym == SDLK_w || event.key.keysym.sym == SDLK_UP) { //pan up
					screen->camera.y -= panZoomSpeed;
				}
				if (event.key.keysym.sym == SDLK_a || event.key.keysym.sym == SDLK_LEFT) { //pan left
					screen->camera.x -= panZoomSpeed;
				}
				if (event.key.keysym.sym == SDLK_s || event.key.keysym.sym == SDLK_DOWN) { //pan down
					screen->camera.y += panZoomSpeed;
				}
				if (event.key.keysym.sym == SDLK_d || event.key.keysym.sym == SDLK_RIGHT) { // pan right
					screen->camera.x += panZoomSpeed;
				}
				if (event.key.keysym.sym == SDLK_q) { //zoom out
					if (screen->camera.zoom > 0) {
						screen->camera.zoom -= panZoomSpeed/100;
					}
				}
				if (event.key.keysym.sym == SDLK_e) { // zoom in 
					if (screen->camera.zoom < 2.0f) {
						screen->camera.zoom += panZoomSpeed/100;
					}
				}
				if (event.key.keysym.sym == SDLK_x) { //reset camera
					screen->camera.zoom = 1;
					screen->camera.x = 0;
					screen->camera.y = 0;
				}
			}
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				if (event.button.button == SDL_BUTTON_LEFT) {
					file << "Mouse Position: " << mx << ", " << my << endl;
				}
			}
		}

		SDL_GetMouseState(&mx, &my);

		if (keystates[SDL_SCANCODE_A] || keystates[SDL_SCANCODE_LEFT])
			x -= 2.0f;
		else if (keystates[SDL_SCANCODE_D] || keystates[SDL_SCANCODE_RIGHT])
			x += 2.0f;
		if (keystates[SDL_SCANCODE_W] || keystates[SDL_SCANCODE_UP])
			y -= 2.0f;
		else if (keystates[SDL_SCANCODE_S] || keystates[SDL_SCANCODE_DOWN])
			y += 2.0f;

		if (drawType == picture ) { //picture mode
			GPU_ClearRGB(screen, 100, 255, 100);
			GPU_Blit(smileImage, nullptr, screen, x, y);
		}

		if (drawType == plot) { //plot mode
			GPU_ClearRGB(screen, 255, 255, 255);
			GPU_SetLineThickness(3.0f);
			GPU_Line(screen, 25, 575, 775, 575, GPU_MakeColor(255, 0, 0, 255));
			GPU_Line(screen, 25, 25, 25, 575, GPU_MakeColor(0, 255, 0, 255));

			for (x = 25; x < 775; x+=0.01f) {
				float equations[30] = { //Equation coresponding with name of interpolation (all from easings.net)
			1 - cos((x / 800 * M_PI) / 2), //Ease in Sine
			sin((x / 800 * M_PI) / 2), //Ease out Sine
			-(cos(M_PI * x / 800) - 1) / 2, //Ease in out Sine

			(x / 800) * (x / 800), //Ease in quad
			1 - (1 - (x / 800)) * (1 - (x / 800)), //Ease out Quad
			(x / 800) < 0.5 ? 2 * (x / 800) * (x / 800) : 1 - pow(-2 * (x / 800) + 2, 2) / 2, //Ease in out Quad

			(x / 800) * (x / 800) * (x / 800), //ease in cubic
			1 - pow(1 - (x / 800), 3), //ease out cubic
			(x / 800) < 0.5 ? 4 * (x / 800) * (x / 800) * (x / 800) : 1 - pow(-2 * (x / 800) + 2, 3) / 2, //Ease in out cubic

			(x / 800) * (x / 800) * (x / 800) * (x / 800), //Ease in Quart
			1 - pow(1 - (x / 800), 4), //Ease out Quart
			(x / 800) < 0.5 ? 8 * (x / 800) * (x / 800) * (x / 800) * (x / 800) : 1 - pow(-2 * (x / 800) + 2, 4) / 2, //Ease in out Quart

			(x / 800) * (x / 800) * (x / 800) * (x / 800) * (x / 800), //Ease in Quint
			1 - pow(1 - (x / 800), 5), //Ease out Quint
			(x / 800) < 0.5 ? 16 * (x / 800) * (x / 800) * (x / 800) * (x / 800) * (x / 800) : 1 - pow(-2 * (x / 800) + 2, 5) / 2, //Ease in out Quint

			(x / 800) == 0 ? 0 : pow(2, 10 * (x / 800) - 10), //Ease in Expo
			(x / 800) == 1 ? 1 : 1 - pow(2, -10 * (x / 800)), //Ease out Expo
			(x / 800) == 0 ? 0 : (x / 800) == 1 ? 1 : (x / 800) < 0.5 ? pow(2, 20 * (x / 800) - 10) / 2 : (2 - pow(2, -20 * (x / 800) + 10)) / 2, //Ease in out Expo

			1 - sqrt(1 - pow((x / 800), 2)), //Ease in Circ
			sqrt(1 - pow((x / 800) - 1, 2)), //Ease out Circ
			(x / 800) < 0.5 ? (1 - sqrt(1 - pow(2 * (x / 800), 2))) / 2 : (sqrt(1 - pow(-2 * (x / 800) + 2, 2)) + 1) / 2, //Ease in out Circ

			c3 * (x / 800) * (x / 800) * (x / 800) - c1 * (x / 800) * (x / 800), //Ease in Back
			1 + c3 * pow((x / 800) - 1, 3) + c1 * pow((x / 800) - 1, 2), //Ease out Back
			(x / 800) < 0.5 ? (pow(2 * (x / 800), 2) * ((c2 + 1) * 2 * (x / 800) - c2)) / 2 : (pow(2 * (x / 800) - 2, 2) * ((c2 + 1) * ((x / 800) * 2 - 2) + c2) + 2) / 2, //Ease in out Back

			(x / 800) == 0 ? 0 : (x / 800) == 1 ? 1 : -pow(2, 10 * (x / 800) - 10) * sin(((x / 800) * 10 - 10.75) * c4), //Ease in Elastic
			(x / 800) == 0 ? 0 : (x / 800) == 1 ? 1 : pow(2, -10 * (x / 800)) * sin(((x / 800) * 10 - 0.75) * c4) + 1, //Ease out Elastic
			(x / 800) == 0 ? 0 : (x / 800) == 1 ? 1 : (x / 800) < 0.5 ? -(pow(2, 20 * (x / 800) - 10) * sin((20 * (x / 800) - 11.125) * c5)) / 2 : (pow(2, -20 * x + 10) * sin((20 * x - 11.125) * c5)) / 2 + 1, //Ease in out Elastic

			1 - GetEaseOutBounce(1 - (x / 800)), //Ease in Bounce
			GetEaseOutBounce(x / 800), //Ease Out Bounce
			(x / 800) < 0.5 ? (1 - GetEaseOutBounce(1 - 2 * (x / 800))) / 2 : (1 + GetEaseOutBounce(2 * (x / 800) - 1)) / 2 //Ease In Out Bounce
				};
				y = (equations[cycle] * 550) + 25;
				GPU_Pixel(screen, x, y, GPU_MakeColor(0, 0, 255, 255));
			}

			for (x = 25; x < 776; x += 75) {
				GPU_Line(screen, x, 25, x, 575, GPU_MakeColor(0, 0, 0, 52.28));
				font.draw(screen, x-15, 575, "(%d, 0)", (int)((x-25)/75));
			}
			for (y = 25; y < 576; y += 55) {
				GPU_Line(screen, 25, y, 775, y, GPU_MakeColor(0, 0, 0, 52.28));
				font.draw(screen, 0, y-15, "(%d, 0)", (int)((610-y) / 55));
			}
		}

		font.draw(screen, 50, 10, "%s", interpType.c_str());
		font.draw(screen, screen->w - 50, 10, NFont::AlignEnum::RIGHT, "Mouse: (%d, %d)", mx, my);

		GPU_Flip(screen);

		SDL_Delay(1);
	}
	file.close();
	GPU_FreeImage(smileImage);
	font.free();

	GPU_Quit();
	return 0;
}
