#pragma once

#include <memory>
#include <vector>

#include "Vector2.h"
#include "Transform.h"
#include "VertexHolder.h"
#include "PolygonRenderer.h"
#include "Rigidbody.h"
#include "PolygonCollider.h"

class Entity
{
public:
	std::shared_ptr<Transform> transform;
	std::shared_ptr<VertexHolder> vertexHolder;
	std::shared_ptr<PolygonRenderer> renderer;
	std::shared_ptr<Rigidbody> rigidbody;
	std::shared_ptr<PolygonCollider> collider;


	Entity(std::vector<Vector2> vertices);

};

