#pragma once

#include "Entity.h"
#include "SDL.h"

class GameState
{
public:

	enum class CollisionModeEnum { None, Displace, DisplaceAndVelocity };
	CollisionModeEnum collisionMode;

	Entity A;
	Entity B;

	float moveSpeed;
	bool done;

	bool gotCollision;

	Vector2 MTV;



	GameState();

	void HandleEvent(const SDL_Event& event);

	void HandleInputState(const Uint8* keystates, float dt);

	void PhysicsUpdate(float dt, GPU_Target* screen);

	void Render(GPU_Target* screen);
};