#include "PolygonRenderer.h"
using namespace std;

PolygonRenderer::PolygonRenderer(shared_ptr<Transform> newTransform, shared_ptr<VertexHolder> newVertices)
	: transform(newTransform), vertices(newVertices)
{
	color = GPU_MakeColor(255, 0, 0, 255);
}

void PolygonRenderer::Draw(GPU_Target* target)
{
	auto points = this->vertices->Get();
	if (points.size() < 2)
		return;

	GPU_PushMatrix();
	GPU_Translate(transform->position.x, transform->position.y, 0.0f);
	GPU_Rotate(transform->angleDegrees, 0.0f, 0.0f, 1.0f);

	// TODO: Loop through all points and draw lines between them
	for (auto i = 0; i < points.size()-1; i++)
		GPU_Line(target, points[i].x, points[i].y, points[i + 1].x, points[i + 1].y, color);
	GPU_Line(target, points[0].x, points[0].y, points[points.size()-1].x, points[points.size()-1].y, color);


	GPU_PopMatrix();
}