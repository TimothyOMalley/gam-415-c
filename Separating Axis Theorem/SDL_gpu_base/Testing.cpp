#include "Testing.h"

void TestPolygonRender(GPU_Target* target) {
	std::vector<Vector2> vert;
	vert.push_back(Vector2(2, 2));
	vert.push_back(Vector2(2, 7));
	vert.push_back(Vector2(7, 7));
	vert.push_back(Vector2(7, 2));


	std::shared_ptr<Transform> trans(new Transform());
	std::shared_ptr<VertexHolder> vertHold(new VertexHolder(vert));
	PolygonRenderer testPolyRend = PolygonRenderer(trans, vertHold);
	testPolyRend.Draw(target);
}