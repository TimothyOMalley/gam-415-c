#include "Rigidbody.h"


Rigidbody::Rigidbody()
	: mass(1.0f), drag(0.1f), isStatic(false)
{}

void Rigidbody::Tick(Vector2& position, float dt)
{
	if (isStatic)
	{
		velocity = Vector2();
	}
	else
	{
		Vector2 accel = forceToApply / mass;
		velocity += accel * dt;
		position += velocity * dt;
		if (velocity.Magnitude() > drag)
			velocity -= velocity.Normalized() * drag;
		else
			velocity = Vector2();
	}

	forceToApply = Vector2();
}

