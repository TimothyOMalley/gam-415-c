#include "VertexHolder.h"
#include "SDL_gpu.h"
using namespace std;

VertexHolder::VertexHolder(vector<Vector2> newVertices)
	: vertices(newVertices)
{}

vector<Vector2>& VertexHolder::Get()
{
	return vertices;
}

const vector<Vector2>& VertexHolder::Get() const
{
	return vertices;
}

void VertexHolder::Set(vector<Vector2> newVertices)
{
	vertices = newVertices;
}

vector<Vector2> VertexHolder::GetTransformed(shared_ptr<Transform> transform)
{
	vector<Vector2> result = vertices;
	if (result.size() < 1)
		return result;

	float mat[16];
	float v[3];
	GPU_MatrixIdentity(mat);
	GPU_MatrixTranslate(mat, transform->position.x, transform->position.y, 0.0f);
	GPU_MatrixRotate(mat, transform->angleDegrees, 0.0f, 0.0f, 1.0f);
	for (size_t i = 0; i < result.size(); ++i)
	{
		v[0] = result[i].x;
		v[1] = result[i].y;
		v[2] = 0.0f;
		GPU_VectorApplyMatrix(v, mat);
		result[i] = { v[0], v[1] };
	}

	return result;
}