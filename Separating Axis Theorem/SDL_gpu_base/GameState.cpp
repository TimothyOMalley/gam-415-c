#include "GameState.h"
#include "SDL_gpu.h"
#include "Entity.h"


void DrawArrow(GPU_Target* target, float x1, float y1, float x2, float y2, SDL_Color color)
{
	GPU_Line(target, x1, y1, x2, y2, color);

	// Arrow head
	Vector2 a = { x1, y1 };
	Vector2 b = { x2, y2 };
	float dist = Vector2::Distance(a, b);
	if (dist > 1.0f)
	{
		Vector2 sep = b - a;
		sep.Normalize();
		float length = 15;
		if (dist < 15)
			length = dist * (dist / 15);

		float c[3] = { sep.x, sep.y, 0.0f };
		float mat[16];
		GPU_MatrixIdentity(mat);
		GPU_MatrixRotate(mat, 15.0f, 0, 0, 1);
		GPU_VectorApplyMatrix(c, mat);
		GPU_Line(target, x2 - length * c[0], y2 - length * c[1], x2, y2, color);

		c[0] = sep.x;
		c[1] = sep.y;
		c[2] = 0.0f;
		GPU_MatrixIdentity(mat);
		GPU_MatrixRotate(mat, -15.0f, 0, 0, 1);
		GPU_VectorApplyMatrix(c, mat);
		GPU_Line(target, x2 - length * c[0], y2 - length * c[1], x2, y2, color);
	}
}

void ClampToScreen(Entity& e, GPU_Target* screen)
{
	if (e.transform->position.x < 0)
	{
		e.transform->position.x = 0;
		e.rigidbody->velocity.x = -e.rigidbody->velocity.x;
	}
	else if (e.transform->position.x > screen->w)
	{
		e.transform->position.x = screen->w;
		e.rigidbody->velocity.x = -e.rigidbody->velocity.x;
	}

	if (e.transform->position.y < 0)
	{
		e.transform->position.y = 0;
		e.rigidbody->velocity.y = -e.rigidbody->velocity.y;
	}
	else if (e.transform->position.y > screen->h)
	{
		e.transform->position.y = screen->h;
		e.rigidbody->velocity.y = -e.rigidbody->velocity.y;
	}
}













GameState::GameState()
	: collisionMode(CollisionModeEnum::None),
	A({ {-100, -100}, {100, -100}, {100, 100}, {-100, 100} }),
	B({ {-100, -100}, {100, -100}, {100, 100}, {-100, 100} })
{
	A.transform->position = { 200, 300 };
	B.transform->position = { 500, 300 };
	B.renderer->color = GPU_MakeColor(0, 255, 0, 255);

	moveSpeed = 300.0f;
	done = false;

	gotCollision = false;
}

void GameState::HandleEvent(const SDL_Event& event)
{
	if (event.type == SDL_QUIT)
		done = true;
	if (event.type == SDL_KEYDOWN)
	{
		if (event.key.keysym.sym == SDLK_ESCAPE)
			done = true;
		if (event.key.keysym.sym == SDLK_SPACE)
		{
			int newMode = (int)collisionMode + 1;
			if (newMode > 2)
				newMode = 0;
			collisionMode = (GameState::CollisionModeEnum)newMode;
		}
	}
}

void GameState::HandleInputState(const Uint8* keystates, float dt)
{
	if (collisionMode == GameState::CollisionModeEnum::DisplaceAndVelocity)
	{
		if (keystates[SDL_SCANCODE_A])
			A.rigidbody->velocity.x -= moveSpeed * dt;
		else if (keystates[SDL_SCANCODE_D])
			A.rigidbody->velocity.x += moveSpeed * dt;
		if (keystates[SDL_SCANCODE_W])
			A.rigidbody->velocity.y -= moveSpeed * dt;
		else if (keystates[SDL_SCANCODE_S])
			A.rigidbody->velocity.y += moveSpeed * dt;

		if (keystates[SDL_SCANCODE_LEFT])
			B.rigidbody->velocity.x -= moveSpeed * dt;
		else if (keystates[SDL_SCANCODE_RIGHT])
			B.rigidbody->velocity.x += moveSpeed * dt;
		if (keystates[SDL_SCANCODE_UP])
			B.rigidbody->velocity.y -= moveSpeed * dt;
		else if (keystates[SDL_SCANCODE_DOWN])
			B.rigidbody->velocity.y += moveSpeed * dt;
	}
	else
	{
		if (keystates[SDL_SCANCODE_A])
			A.transform->position.x -= moveSpeed * dt;
		else if (keystates[SDL_SCANCODE_D])
			A.transform->position.x += moveSpeed * dt;
		if (keystates[SDL_SCANCODE_W])
			A.transform->position.y -= moveSpeed * dt;
		else if (keystates[SDL_SCANCODE_S])
			A.transform->position.y += moveSpeed * dt;

		if (keystates[SDL_SCANCODE_LEFT])
			B.transform->position.x -= moveSpeed * dt;
		else if (keystates[SDL_SCANCODE_RIGHT])
			B.transform->position.x += moveSpeed * dt;
		if (keystates[SDL_SCANCODE_UP])
			B.transform->position.y -= moveSpeed * dt;
		else if (keystates[SDL_SCANCODE_DOWN])
			B.transform->position.y += moveSpeed * dt;
	}

	if (keystates[SDL_SCANCODE_Q])
		A.transform->angleDegrees -= 120.0f * dt;
	else if (keystates[SDL_SCANCODE_E])
		A.transform->angleDegrees += 120.0f * dt;

	if (keystates[SDL_SCANCODE_COMMA])
		B.transform->angleDegrees -= 120.0f * dt;
	else if (keystates[SDL_SCANCODE_PERIOD])
		B.transform->angleDegrees += 120.0f * dt;
}

void GameState::PhysicsUpdate(float dt, GPU_Target* screen)
{
	A.rigidbody->Tick(A.transform->position, dt);
	B.rigidbody->Tick(B.transform->position, dt);


	gotCollision = false;

	if (PolygonCollider::SATGetMTV(A.collider, B.collider, MTV))
	{
		if (collisionMode == CollisionModeEnum::Displace)
		{
			// TODO: Use the MTV to move each object out of overlap
			A.transform->position += MTV;
			B.transform->position -= MTV;
		}
		else if (collisionMode == CollisionModeEnum::DisplaceAndVelocity)
		{
			// TODO: Use the MTV to move each object out of overlap, then apply a velocity away from each other
			A.transform->position += MTV;
			B.transform->position -= MTV;
			Vector2 temp = A.rigidbody->velocity;
			A.rigidbody->velocity = B.rigidbody->velocity;
			B.rigidbody->velocity = temp;
		}
		gotCollision = true;
	}

	ClampToScreen(A, screen);
	ClampToScreen(B, screen);
}

void GameState::Render(GPU_Target* screen)
{
	GPU_ClearRGB(screen, 0, 0, 0);

	A.renderer->Draw(screen);
	B.renderer->Draw(screen);

	if (gotCollision)
	{
		Vector2 perp = Vector2(-MTV.y, MTV.x);
		Vector2 startPos = { screen->w / 2.0f, screen->h - 100.0f };
		DrawArrow(screen, startPos.x, startPos.y, startPos.x + MTV.x, startPos.y + MTV.y, GPU_MakeColor(200, 50, 150, 255));
	}
}
