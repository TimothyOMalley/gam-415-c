#pragma once
#include <memory>
#include "Transform.h"
#include "VertexHolder.h"
#include "SDL_gpu.h"

class PolygonRenderer
{
public:
	std::shared_ptr<Transform> transform;
	std::shared_ptr<VertexHolder> vertices;
	SDL_Color color;

	PolygonRenderer(std::shared_ptr<Transform> newTransform, std::shared_ptr<VertexHolder> newVertices);

	void Draw(GPU_Target* target);
};