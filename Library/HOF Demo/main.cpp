#include "CustomLibrary.h"
#include <iostream>
#include <vector>
using namespace std;


bool NumberIsDivisableByFive(int number) { return (number % 5 == 0); }
bool IfNumberIsTwentyThree(int num) { return (num == 23); }
void PrintVector(vector<int> vec) { for (int i = 0; i < (int)(vec.size()); i++){cout << vec[i] << " ";} }
int AddThree(int num) { return num + 3; }
int AddNums(int num, int num2) { return num + num2; }

int main() {
	vector<int> exampleVecOne = vector<int>();
	exampleVecOne.push_back(7);
	exampleVecOne.push_back(10);
	exampleVecOne.push_back(5);
	exampleVecOne.push_back(6);
	exampleVecOne.push_back(23);
	exampleVecOne.push_back(45);

	cout << "Example of Higher Order Functions library:" << endl << endl;
	cout << "Starting Vector: ";
	PrintVector(exampleVecOne);

	cout << "\n     First number divisible by 5: " << FindIf(exampleVecOne, NumberIsDivisableByFive);
	cout << "\n     Index of '23' in the vector: " << IndexOf(exampleVecOne, IfNumberIsTwentyThree );
	cout << "\n     Each element of vector +3  : "; PrintVector(Map(exampleVecOne, AddThree));
	cout << "\n     All values combined to one : " << ReduceLeft(exampleVecOne, 7, AddNums);
	cout << "\n     First number NOT divis by 5: " << FindIf(exampleVecOne, Negate(NumberIsDivisableByFive));

}