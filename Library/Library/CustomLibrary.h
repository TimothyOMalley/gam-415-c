#pragma once
#include <functional>
#include <vector>
#ifndef EXPORT_LIBRARY
	#if defined(BUILD_LIBRARY)
		#define EXPORT_LIBRARY __declspec(dllexport)
	#else
		#define EXPORT_LIBRARY __declspec(dllimport)
	#endif
#endif

EXPORT_LIBRARY int FindIf(std::vector<int> v, std::function<bool(int)> f);

EXPORT_LIBRARY int IndexOf(std::vector<int> v, std::function<bool(int)> f);

EXPORT_LIBRARY std::vector<int> Map(std::vector<int> v, std::function<int(int)> f);

EXPORT_LIBRARY int ReduceLeft(std::vector<int> v, int initial, std::function<int(int, int)> f);

EXPORT_LIBRARY std::function<bool(int)> Negate(std::function<bool(int)> f);

inline int GetHeaderVersion() {
	return 1;
}

EXPORT_LIBRARY int GetLibraryVersion();