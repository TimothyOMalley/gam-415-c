#include "CustomLibrary.h"

using namespace std;

int FindIf(vector<int> v, function<bool(int)> f) {
	for (int i = 0; i < (int)(v.size()); i++) {
		if (f(v[i])) {
			return v[i];
		}
	}
	throw;
}

int IndexOf(vector<int> v, function<bool(int)> f) {
	for (int i = 0; i < (int)(v.size()); i++) {
		if (f(v[i])) {
			return i;
		}
	}
	throw;
}

vector<int> Map(vector<int> v, function<int(int)> f) {
	vector<int> temp;
	for (int i = 0; i < (int)(v.size()); i++) {
		temp.push_back(f(v[i]));
	}
	return temp;
}

int ReduceLeft(vector<int> v, int initial, function<int(int, int)> f) {
	int returnVal = -1;
	for (int i = IndexOf(v, [initial](int i) { return(initial == i); }); i < (int)(v.size()); i++) {
		returnVal = f(returnVal, v[i]);
	}
	return returnVal;
}

function<bool(int)> Negate(function<bool(int)> f) {
	return [f](int i) {return !f(i); };
}

int GetLibraryVersion() {
	return GetHeaderVersion();
}