#include <iostream>
#include <conio.h>
#include <functional>

using namespace std;

char *lambda;

auto printLambda = [](char _lambda) {
	cout << lambda << endl;
};

auto setLambda = [](char _lambda) {
	if (lambda != nullptr)
		lambda = &_lambda;
	else {
		lambda = new char();
		lambda = &_lambda;
	}
};

void GetOrSet(char var,function<void(char)> callBack) {
	callBack(var);
}

int main()
{
	while (true) {
		switch (_getch()) {
		case 'a':
			GetOrSet('a', setLambda);
			break;
		case 'b':
			GetOrSet('b', setLambda);
			break;
		case 'c':
			GetOrSet('c', setLambda);
			break;
		case ' ':
			GetOrSet(' ', printLambda);
			break;
		case 27:
			return 1;
		}
	}
}
