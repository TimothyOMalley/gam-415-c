#include "SDL.h"
#include <functional>
#include "AudioBuffer.h"
#include "Random.h"
using namespace std;
Sint16 GetSineSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// Figure out how the sample rate relates to real time
	int samplesPerPeriod = buffer.GetSampleRate() / frequency;

	// Tells us how far along the wave we have traveled
	float waveSegment = sampleIndex / float(samplesPerPeriod);

	return volume * sin(2 * M_PI * waveSegment);
}

Sint16 GetSquareSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// TODO: Implement me!
	int samplesPerPeriod = buffer.GetSampleRate() / frequency;

	float waveSegment = sampleIndex / float(samplesPerPeriod);

	float thingToUse = sin(2 * M_PI * waveSegment);

	if (thingToUse <= 0) { return volume * floor(thingToUse); }
	else { return volume * ceil(thingToUse); }
}

Sint16 GetTriangleSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// Figure out how the sample rate relates to real time
	int samplesPerPeriod = buffer.GetSampleRate() / frequency;

	// Tells us how far along the wave we have traveled
	float waveSegment = sampleIndex / float(samplesPerPeriod);

	int sampleInPeriod = sampleIndex % samplesPerPeriod;

	int samplesPerHalfPeriod = samplesPerPeriod / 2;

	float normalizedSampleInHalfPeriod = float(sampleInPeriod) / samplesPerHalfPeriod;

	float absSample = fabsf(normalizedSampleInHalfPeriod - 1.0f);
	//return(volume * abs(sin(2 * M_PI * waveSegment))); diamonds!
	return volume * 2 * absSample - 1.0f;
}

Sint16 GetSawtoothSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// TODO: Implement me!
	// Figure out how the sample rate relates to real time
	int samplesPerPeriod = buffer.GetSampleRate() / frequency;

	// Tells us how far along the wave we have traveled
	float waveSegment = sampleIndex / float(samplesPerPeriod);

	static int counter = 0;
	counter++;
	if (sampleIndex % samplesPerPeriod == 0)
		counter = 0;
	return volume * waveSegment * counter;
}

Sint16 GetNoiseSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// TODO: Implement me!
	static Random rand = Random();
	return rand.Float(-1, 1) * volume;
}