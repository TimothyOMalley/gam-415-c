#include "SimpleGraphics.h"
#include <iostream>
#include <string>
#include <functional>
#include "SDL.h"
using namespace std;

SDL_Renderer* renderer = nullptr;
SDL_Window* window = nullptr;
uint32_t pixelDelay = 0;


bool SG_Initialize(const std::string title, int width, int height)
{
	if (renderer != nullptr)
		return true;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		cout << "SDL failed to initialize! (" << SDL_GetError() << ")" << endl;
		return false;
	}

	window = SDL_CreateWindow(title.c_str(),
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		width, height, SDL_WINDOW_OPENGL);

	if (window == nullptr)
	{
		cout << "Error creating a window! (" << SDL_GetError() << ")" << endl;
		SDL_Quit();
		return false;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	if (renderer == nullptr)
	{
		cout << "Error creating a renderer! (" << SDL_GetError() << ")" << endl;
		SDL_DestroyWindow(window);
		SDL_Quit();
		return false;
	}

	

	return true;
}

void SG_CleanUp()
{
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();
}


DLL_EXPORT void SG_SetPixelDelay(uint32_t milliseconds)
{
	pixelDelay = milliseconds;
}


void SG_Run(std::function<void(float)> renderCallback)
{
	float dt = 0.0f;
	Uint32 frameStart;
	Uint32 frameEnd;

	SDL_Event event;
	bool done = false;
	while (!done)
	{
		frameStart = SDL_GetTicks();

		// Process input events and keys pressed this frame
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				done = true;
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
					done = true;
			}
		}
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderClear(renderer);

		renderCallback(dt);

		if (pixelDelay > 0)
			pixelDelay = 0;

		SDL_RenderPresent(renderer);


		// Give some time back to the OS
		SDL_Delay(1);

		frameEnd = SDL_GetTicks();

		dt = (frameEnd - frameStart) / 1000.0f;
	}
}

// Warning: Very unoptimized/slow to render individual pixels
void SG_SetPixel(int x, int y, uint8_t red, uint8_t green, uint8_t blue)
{
	SDL_SetRenderDrawColor(renderer, red, green, blue, 0xFF);
	SDL_RenderDrawPoint(renderer, x, y);

	if (pixelDelay > 0)
	{
		// Show the result and wait...
		SDL_RenderPresent(renderer);
		SDL_Delay(pixelDelay);
	}
}














struct Image::pimpl
{
	SDL_Renderer* renderer;
	SDL_Texture* texture;

	pimpl()
	{
		renderer = nullptr;
		texture = nullptr;
	}
};

Image::Image(int newWidth, int newHeight)
{
	width = newWidth;
	height = newHeight;

	impl = new pimpl();

	impl->renderer = renderer;
	impl->texture = SDL_CreateTexture(impl->renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_TARGET, width, height);
	if (impl->texture == nullptr)
	{
		cout << "Error creating a texture! (" << SDL_GetError() << ")" << endl;
	}
}

Image::Image(const std::string& filename)
{
	impl = new pimpl();

	impl->renderer = renderer;

	SDL_Surface* surface = SDL_LoadBMP(filename.c_str());
	if (surface == nullptr)
	{
		cout << "Error loading surface! (" << SDL_GetError() << ")" << endl;
		width = 0;
		height = 0;
		return;
	}

	width = surface->w;
	height = surface->h;


	// Copy the image to a renderable texture
	impl->texture = SDL_CreateTexture(impl->renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_TARGET, width, height);

	if (impl->texture == nullptr)
	{
		cout << "Error creating a texture! (" << SDL_GetError() << ")" << endl;
		SDL_FreeSurface(surface);
		return;
	}

	// Copy surface onto new texture (yuck...)

	// New surface with the right format
	SDL_Surface* surf = SDL_CreateRGBSurfaceWithFormat(SDL_SWSURFACE, width, height, 24, SDL_PIXELFORMAT_RGB888);
	SDL_BlitSurface(surface, nullptr, surf, nullptr);
	SDL_FreeSurface(surface);

	// Update the texture
	SDL_Rect rect = {0, 0, width, height};
	SDL_UpdateTexture(impl->texture, &rect, surf->pixels, surf->pitch);
	SDL_FreeSurface(surf);
}

Image::~Image()
{
	SDL_DestroyTexture(impl->texture);
	delete impl;
}


void Image::Free()
{
	SDL_DestroyTexture(impl->texture);
	impl->texture = nullptr;
}

int Image::GetWidth() const
{
	return width;
}

int Image::GetHeight() const
{
	return height;
}

void Image::SetAsTarget()
{
	SDL_SetRenderTarget(impl->renderer, impl->texture);
}

void Image::UnsetAsTarget()
{
	SDL_SetRenderTarget(impl->renderer, nullptr);
}

// Warning: Very unoptimized/slow to render individual pixels
void Image::SetPixel(int x, int y, uint8_t red, uint8_t green, uint8_t blue)
{
	SDL_SetRenderDrawColor(impl->renderer, red, green, blue, 0xFF);
	SDL_RenderDrawPoint(impl->renderer, x, y);
}

void Image::DrawToScreen(float x, float y)
{
	SDL_Rect destinationArea = { (int)x, (int)y, width, height };
	SDL_RenderCopy(impl->renderer, impl->texture, nullptr, &destinationArea);
}

void Image::DrawToScreen(float x, float y, float degrees)
{
	SDL_Rect destinationArea = { (int)x, (int)y, width, height };
	SDL_RenderCopyEx(impl->renderer, impl->texture, nullptr, &destinationArea, degrees, nullptr, SDL_FLIP_NONE);
}
