#include <iostream>
#include <string>
#include "SimpleGraphics.h"
#include <math.h>
#include <vector>
#include <unordered_map>
using namespace std;



class Sprite
{
public:
	float x, y;
	float degrees;
	Image image;

	Sprite(const std::string& filename)
		: image(filename)
	{
		x = 0.0f;
		y = 0.0f;
		degrees = 0.0f;
	}

	void Draw()
	{
		image.DrawToScreen(x, y, degrees);
	}
};



void DrawGradient()
{
	int numRows = 800;
	int numColumns = 800;
	for (int row = 0; row < numRows; ++row)
	{
		for (int column = 0; column < numColumns; ++column)
		{
			uint8_t red = (float(row/numRows)) * 255;
			uint8_t green = 255;
			uint8_t blue = 255;

			SG_SetPixel(column, row, red, green, blue);
		}
	}
}

void DrawHorizontalLine(int x1, int x2, int y) {
	for (int x = fmin(x1,x2) ; x < fmax(x1,x2); x++) {
		SG_SetPixel(x, y, 0, 0, 255);
	}
}

void DrawHorizontalLine(int x1, int x2, int y,int r,int g,int b) {
	for (int x = fmin(x1, x2); x < fmax(x1, x2); x++) {
		SG_SetPixel(x, y, r, g, b);
	}
}

void DrawVericalLine(int y1, int y2, int x) {
	for (int y = fmin(y1, y2); y < fmax(y1, y2); y++) {
		SG_SetPixel(x, y, 0, 255, 0);
	}
}

void DrawVericalLine(int y1, int y2, int x, int r, int g, int b) {
	for (int y = fmin(y1, y2); y < fmax(y1, y2); y++) {
		SG_SetPixel(x, y, r, g, b);
	}
}

void DrawRectangleOutline(int x1, int y1, int x2, int y2) {
	DrawHorizontalLine(x1, x2, y1);
	DrawHorizontalLine(x1, x2, y2);
	DrawVericalLine(y1, y2, x1);
	DrawVericalLine(y1, y2, x2);
}

void DrawRectangleOutline(int x1, int y1, int x2, int y2, int r, int g, int b) {
	DrawHorizontalLine(x1, x2, y1, r, g, b);
	DrawHorizontalLine(x1, x2, y2, r, g, b);
	DrawVericalLine(y1, y2, x1, r, g, b);
	DrawVericalLine(y1, y2, x2, r, g, b);
}

void DrawFilledRectangle(int x1, int y1, int x2, int y2) {
	for (int x = fmin(x1, x2); x < fmax(x1, x2); x++) {
		for (int y = fmin(y1, y2); y < fmax(y1, y2); y++) {
			SG_SetPixel(x, y, 255, 0, 0);
		}
	}
}

void DrawFilledRectangle(int x1, int y1, int x2, int y2, int r,int g, int b) {
	for (int x = fmin(x1, x2); x < fmax(x1, x2); x++) {
		for (int y = fmin(y1, y2); y < fmax(y1, y2); y++) {
			SG_SetPixel(x, y, r,g,b);
		}
	}
}

void DrawBresenhamLine(int x1, int y1, int x2, int y2) { //50 50 150 75
	/*
	determine rise (y2-y1)
	determine run (x2-x1)
	determine slope (rise/run)
	edge case:
		if(rise == 0) {DrawHorizontal}
	edge case:
		if(run == 0) {DrawVertical}
	if slope < 1
		for(x1->x2)
		if(slope*passes >=1)
			y++
	if slope > 1
		invert slope (run/rise)
		for(y1->y2)
		if(slope*passes)
	*/

	float rise = y2 - y1;
	float run = x2 - x1;
	float slope = rise / run;

	if (rise == 0)
		return DrawHorizontalLine(x1, x2, y1);
	if (run == 0)
		return DrawVericalLine(y1, y2, x1);
	if (abs(slope) < 1) {
		int y = 0;
		for (int i = fmin(x1, x2); i < fmax(x1, x2); i++) {
			y = (int)((i - fmin(x1, x2)) * slope); //step*slope
			if(slope > 0)
				SG_SetPixel(i, y + fmin(y1, y2), 255, 0, 0);
			if(slope < 0)
				SG_SetPixel(i, y + fmax(y1, y2), 255, 0, 0);
		}
	}
	if (abs(slope) == 1) {
		if (slope > 0) { //slope is positive
			for (int i = 0; i < (fmax(x1, x2) - fmin(x1, x2)); i++) {
				SG_SetPixel(i + fmin(x1, x2), i + fmin(y1, y2), 255, 0, 0);
			}
		}
		if (slope < 0) { //slope is negative
			for (int i = 0; i < (fmax(x1, x2) - fmin(x1, x2)); i++) {
				SG_SetPixel(i + fmin(x1, x2), fmax(y1, y2) - i, 255, 0, 0);
			}
		}
	}
	if (abs(slope) > 1) {
		slope = run / rise;
		int x = 0;
		for (int i = fmin(y1, y2); i < fmax(y1, y2); i++) {
			x = (int)((i - fmin(y1, y2)) * slope);
			if(slope > 0) //slope +
				SG_SetPixel(x + fmin(x1, x2),i, 255, 0, 0);
			if (slope < 0) //slope -
				SG_SetPixel(x + fmax(x1, x2),i, 255, 0, 0);
		}
	}

	SG_SetPixel(x1, y1, 0, 255, 0);
	SG_SetPixel(x2, y2, 0, 255, 0);
}

unordered_map<int,int> GetBresenhamLineInformation(int x1, int y1, int x2, int y2) {
	int size = fmax(x1, x2) - fmin(x1, x2);
	unordered_map<int,int> ret;

	float rise = y2 - y1;
	float run = x2 - x1;
	float slope = rise / run;

	if (rise == 0)
		return ret;
	if (run == 0)
		return ret;
	if (abs(slope) < 1) {
		int y = 0;
		for (int i = fmin(x1, x2); i < fmax(x1, x2); i++) {
			y = (int)((i - fmin(x1, x2)) * slope); //step*slope
			if (slope > 0)
				ret[i] = y + fmin(y1, y2);
			if (slope < 0)
				ret[i] = y + fmax(y1, y2);
		}
	}
	if (abs(slope) == 1) {
		if (slope > 0) { //slope is positive
			for (int i = 0; i < (fmax(x1, x2) - fmin(x1, x2)); i++) {
				ret[i + fmin(x1, x2)] = i + fmin(y1, y2);
			}
		}
		if (slope < 0) { //slope is negative
			for (int i = 0; i < (fmax(x1, x2) - fmin(x1, x2)); i++) {
				ret[i + fmin(x1, x2)] = fmax(y1, y2) - i;
			}
		}
	}
	if (abs(slope) > 1) {
		slope = run / rise;
		int x = 0;
		for (int i = fmin(y1, y2); i < fmax(y1, y2); i++) {
			x = (int)((i - fmin(y1, y2)) * slope);
			if (slope > 0) //slope +
				ret[x + fmin(x1, x2)] = i;
			if (slope < 0) //slope -
				ret[x + fmax(x1, x2)] = i;
		}
	}
	return ret;
}

void DrawBresenhamLine(int x1, int y1, int x2, int y2, int r, int g, int b) { //50 50 150 75
	/*
	determine rise (y2-y1)
	determine run (x2-x1)
	determine slope (rise/run)
	edge case:
		if(rise == 0) {DrawHorizontal}
	edge case:
		if(run == 0) {DrawVertical}
	if slope < 1
		for(x1->x2)
		if(slope*passes >=1)
			y++
	if slope > 1
		invert slope (run/rise)
		for(y1->y2)
		if(slope*passes)
	*/

	float rise = y2 - y1;
	float run = x2 - x1;
	float slope = rise / run;

	if (rise == 0)
		return DrawHorizontalLine(x1, x2, y1, r, g, b);
	if (run == 0)
		return DrawVericalLine(y1, y2, x1, r, g, b);
	if (abs(slope) < 1) {
		int y = 0;
		for (int i = fmin(x1, x2); i < fmax(x1, x2); i++) {
			y = (int)((i - fmin(x1, x2)) * slope); //step*slope
			if (slope > 0)
				SG_SetPixel(i, y + fmin(y1, y2), r,g,b);
			if (slope < 0)
				SG_SetPixel(i, y + fmax(y1, y2), r, g, b);
		}
	}
	if (abs(slope) == 1) {
		if (slope > 0) { //slope is positive
			for (int i = 0; i < (fmax(x1, x2) - fmin(x1, x2)); i++) {
				SG_SetPixel(i + fmin(x1, x2), i + fmin(y1, y2), r, g, b);
			}
		}
		if (slope < 0) { //slope is negative
			for (int i = 0; i < (fmax(x1, x2) - fmin(x1, x2)); i++) {
				SG_SetPixel(i + fmin(x1, x2), fmax(y1, y2) - i, r, g, b);
			}
		}
	}
	if (abs(slope) > 1) {
		slope = run / rise;
		int x = 0;
		for (int i = fmin(y1, y2); i < fmax(y1, y2); i++) {
			x = (int)((i - fmin(y1, y2)) * slope);
			if (slope > 0) //slope +
				SG_SetPixel(x + fmin(x1, x2), i, r, g, b);
			if (slope < 0) //slope -
				SG_SetPixel(x + fmax(x1, x2), i, r, g, b);
		}
	}

	SG_SetPixel(x1, y1, r, g, b);
	SG_SetPixel(x2, y2, r, g, b);
}

void DrawTriangleOutline(int x1, int y1, int x2, int y2, int x3, int y3) {
	DrawBresenhamLine(x1, y1, x2, y2); //50 50 150 75 ; rise = 25 ; run = 100 ; slope = 1/4 
	DrawBresenhamLine(x2, y2, x3, y3); //150 75 50 150 ; rise = 75 ; run = -100 ; slope = -3/4
	DrawBresenhamLine(x3, y3, x1, y1); //50 150 50 50 ; rise = -100 ; run = 0 ; slope = -1/0
}

void DrawFilledTriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
	unordered_map<int,int> lineOne = GetBresenhamLineInformation(x1, y1, x2, y2);
	unordered_map<int, int> lineTwo = GetBresenhamLineInformation(x2, y2, x3, y3);
	unordered_map<int, int> lineThree = GetBresenhamLineInformation(x3, y3, x1, y1);

	for (int i = fmin(x1, x2); i < fmax(x1, x2); i++) {
		if (lineTwo.find(i) != lineTwo.end()) {
			DrawVericalLine(lineOne[i], lineTwo[i], i);
			lineTwo.erase(i);
		}
	}
	for (int i = fmin(x2, x3); i < fmax(x2, x3); i++) {
		if (lineTwo.find(i) != lineTwo.end()) {
			if (lineThree.find(i) != lineThree.end()) {
				DrawVericalLine(lineTwo[i], lineThree[i], i);
				lineThree.erase(i);
			}
		}
	}
	for (int i = fmin(x3, x1); i < fmax(x3, x1); i++) {
		if (lineThree.find(i) != lineThree.end()) {
			if (lineOne.find(i) != lineOne.end()) {
				DrawVericalLine(lineThree[i], lineOne[i], i);
			}
		}
	}
}

void DrawFilledTriangle(int x1, int y1, int x2, int y2, int x3, int y3, int r, int g, int b) {
	unordered_map<int, int> lineOne = GetBresenhamLineInformation(x1, y1, x2, y2);
	unordered_map<int, int> lineTwo = GetBresenhamLineInformation(x2, y2, x3, y3);
	unordered_map<int, int> lineThree = GetBresenhamLineInformation(x3, y3, x1, y1);

	for (int i = fmin(x1, x2); i < fmax(x1, x2); i++) {
		if (lineTwo.find(i) != lineTwo.end()) {
			DrawVericalLine(lineOne[i], lineTwo[i], i, r, g, b);
			lineTwo.erase(i);
		}
	}
	for (int i = fmin(x2, x3); i < fmax(x2, x3); i++) {
		if (lineTwo.find(i) != lineTwo.end()) {
			if (lineThree.find(i) != lineThree.end()) {
				DrawVericalLine(lineTwo[i], lineThree[i], i, r, g, b);
				lineThree.erase(i);
			}
		}
	}
	for (int i = fmin(x3, x1); i < fmax(x3, x1); i++) {
		if (lineThree.find(i) != lineThree.end()) {
			if (lineOne.find(i) != lineOne.end()) {
				DrawVericalLine(lineThree[i], lineOne[i], i, r, g, b);
			}
		}
	}
}

void DrawTriangleOutline(int x1, int y1, int x2, int y2, int x3, int y3, int r, int g, int b) {
	DrawBresenhamLine(x1, y1, x2, y2, r, g, b); //50 50 150 75 ; rise = 25 ; run = 100 ; slope = 1/4 
	DrawBresenhamLine(x2, y2, x3, y3, r, g, b); //150 75 50 150 ; rise = 75 ; run = -100 ; slope = -3/4
	DrawBresenhamLine(x3, y3, x1, y1, r, g, b); //50 150 50 50 ; rise = -100 ; run = 0 ; slope = -1/0
}

void DrawCircleOutLine(int width, int height, int x, int y) {
	for (float i = 0; i < 360; i+=0.125) {
		//x = cos(degree)
		//y = sin(degree)
		SG_SetPixel(((cos(i))*(width/2))+(width/2)+x, ((sin(i))*(height/2))+(height/2)+y, 255, 0, 0);
	}
}

void DrawCircleOutLine(int width, int height, int x, int y, int r, int g, int b) {
	for (float i = 0; i < 360; i += 0.125) {
		//x = cos(degree)
		//y = sin(degree)
		SG_SetPixel(((cos(i)) * (width / 2)) + (width / 2) + x, ((sin(i)) * (height / 2)) + (height / 2) + y, r, g, b);
	}
}

void DrawFilledCircle(int width, int height, int x, int y) {
	DrawCircleOutLine(width, height, x, y);
	if(width != 0)
		return DrawFilledCircle(width - 2, height - 2, x + 1, y + 1);
	//float offset = 59.7;
	//for (float i = 0; i < 360; i++) {
	//	DrawBresenhamLine(((cos(i)) * (width / 2)) + (width / 2) + x, ((sin(i)) * (height / 2)) + (height / 2) + y, ((cos(i+offset)) * (width / 2)) + (width / 2) + x, ((sin(i+offset)) * (height / 2)) + (height / 2) + y);
	//}
}

void DrawFilledCircle(int width, int height, int x, int y, int r, int g, int b) {
	DrawCircleOutLine(width, height, x, y, r, g, b);
	if (width != 0)
		return DrawFilledCircle(width - 2, height - 2, x + 1, y + 1, r, g, b);
	//float offset = 59.7;
	//for (float i = 0; i < 360; i++) {
	//	DrawBresenhamLine(((cos(i)) * (width / 2)) + (width / 2) + x, ((sin(i)) * (height / 2)) + (height / 2) + y, ((cos(i+offset)) * (width / 2)) + (width / 2) + x, ((sin(i+offset)) * (height / 2)) + (height / 2) + y);
	//}
}

void FillImageWithGradient(Image& image)
{
	image.SetAsTarget();

	for (int row = 0; row < image.GetHeight(); ++row)
	{
		for (int column = 0; column < image.GetWidth(); ++column)
		{
			uint8_t red = (float(row) / image.GetHeight()) * 255;
			uint8_t green = 255;
			uint8_t blue = 255;

			image.SetPixel(column, row, red, green, blue);
		}
	}

	image.UnsetAsTarget();
}

void DrawGrid(int width, int height, int x, int y, int freq) {
	int c = 0;
	for (int i = 0; i < fmax(width, height); i+=freq) {
		c = 75;
		if (i % (freq * 5) == 0)
			c = 150;
		if (i % (freq * 10) == 0)
			c = 255;
		if(i<=width)
			DrawVericalLine(y, y + height, i+x, c, c, c);
		if(i<=height)
			DrawHorizontalLine(x, x + width, i+y, c, c, c);
	}
}

void TestBreLine(int o) {
	for (int i = 0; i < 250; i++) {
		DrawBresenhamLine(o + i, o + i, o +(250-i) , o +250); //0 0 249 250 250/249
	}
}


int main(int argc, char* argv[])
{
	SG_Initialize("Image test", 800, 600);

	SG_SetPixelDelay(1);

	
	 
	Sprite smile("smile.bmp");

	SG_Run([&smile](float dt)
		{
			//DrawGrid(800, 600, 0, 0, 10);
			//Brimstone Symbol
			DrawRectangleOutline(375, 25, 425, 400, 255, 0 ,0);
			DrawRectangleOutline(275, 100, 525, 150, 255, 0, 0);
			DrawRectangleOutline(225, 200, 575, 250, 255, 0, 0);

			DrawCircleOutLine(150, 150, 275 - 55, 485 - 150, 255, 0, 0);
			DrawCircleOutLine(150, 150, 515 - 95, 485 - 150, 255, 0, 0);
			
			DrawTriangleOutline(295, 485, 515, 485 - 125, 495, 485 - 150, 255, 0, 0);
			DrawTriangleOutline(295, 485-150, 275, 485 - 125, 495, 485, 255, 0, 0);
			DrawTriangleOutline(340, 390, 400, 400, 340, 450, 255, 0, 0);
			DrawTriangleOutline(460, 390, 400, 400, 460, 450, 255, 0, 0);


			//issac
			//head
			DrawCircleOutLine(200, 200, 400-100, 250-100, 0, 255, 0);
			DrawCircleOutLine(50, 50, 350 - 25, 275 - 50, 0, 255, 0);
			DrawCircleOutLine(50, 50, 450 - 25, 275 - 50, 0, 255, 0);
			//legs
			DrawCircleOutLine(30, 70, 365, 350, 0, 255, 0);
			DrawCircleOutLine(30, 70, 400, 350, 0, 255, 0);
			//torso
			DrawRectangleOutline(360, 320, 440, 380, 0, 255, 0);
			//arms
			DrawCircleOutLine(30, 70, 345, 320,0,255,0);
			DrawCircleOutLine(30, 70, 425, 320, 0, 255, 0);
			//tears
			DrawRectangleOutline(330, 270, 370, 350, 0, 0, 255);
			DrawRectangleOutline(430, 270, 470, 350, 0, 0, 255);
			//mouth
			DrawRectangleOutline(385, 260, 415, 290, 0, 255, 0);
			DrawRectangleOutline(375, 290, 425, 270, 0, 255, 0);
			//teeth
			DrawRectangleOutline(385, 290, 415, 280, 255, 255, 255);

			//Now it all fills
			//Brimstone
				//Overflow
			//DrawFilledRectangle(370, 20, 430, 405, 255, 0, 0);
			//DrawFilledRectangle(270, 95, 530, 155, 255, 0, 0);
			//DrawFilledRectangle(220, 195, 580, 255, 255, 0, 0);
			//DrawFilledCircle(160, 160, 275 - 78 + 20, 485 - 155, 255, 0, 0);
			//DrawFilledCircle(160, 160, 515 - 78 - 20, 485 - 155, 255, 0, 0);
			//DrawFilledTriangle(295, 485, 515, 485 - 125, 495, 485 - 150, 255, 0, 0);
			//DrawFilledTriangle(295, 485-150, 275, 485 - 125, 495, 485, 255, 0, 0);
			//DrawFilledTriangle(340, 390, 400, 400, 340, 450, 255, 0, 0);
			//DrawFilledTriangle(460, 390, 400, 400, 460, 450, 255, 0, 0);
				//Inside
			//DrawFilledRectangle(375, 25, 425, 400, 0, 0, 0);
			//DrawFilledRectangle(275, 100, 525, 150, 0, 0, 0);
			//DrawFilledRectangle(225, 200, 575, 250, 0, 0, 0);
			//DrawFilledCircle(150, 150, 275 - 75 + 20, 485 - 150, 0, 0, 0);
			//DrawFilledCircle(150, 150, 515 - 75 - 20, 485 - 150, 0, 0, 0);

			//DrawFilledCircle(100, 100, 275 - 50 + 20, 485 - 125, 255, 0, 0);
			//DrawFilledCircle(100, 100, 515 - 50 - 20, 485 - 125, 255, 0, 0);
		});

	smile.image.Free();

	SG_CleanUp();

	return 0;
}