#include "Vector2.h"

Vector2::Vector2() {
	x = 0;
	y = 0;
}

Vector2::Vector2(const int x, const int y) {
	this->x = x;
	this->y = y;
}

Vector2 Vector2::operator+(const Vector2 &other) const {
	return Vector2(x + other.x, y + other.y);
}

Vector2 Vector2::operator-(const Vector2 &other) const {
	return Vector2(x-other.x, y-other.y);
}

Vector2 Vector2::operator-() const {
	return Vector2(-x, -y);
}

Vector2 Vector2::operator*(const Vector2 &other) const {
	return Vector2(x * other.x, y * other.y);
}

Vector2 operator*(const float scalar, const Vector2 &other) {
	return Vector2(scalar * other.x, scalar * other.y);
}

bool Vector2::operator==(const Vector2 other) const {
	if (x == other.x)
		if (y == other.y)
			return true;
	return false;
}

int Vector2::GetX() {
	return x;
}

int Vector2::GetY() {
	return y;
}