#pragma once
class Vector2 {
public:
	Vector2(); //constructor
	Vector2(int x, int y); //alt constructor
	Vector2 operator+(const Vector2 &other) const; // + op
	Vector2 operator-(const Vector2 &other) const; // - op
	Vector2 operator-() const;
	Vector2 operator*(const Vector2 &other) const; // * op
	bool operator==(const Vector2 other) const;
	friend Vector2 operator*(const float scalar, const Vector2 &other); //scalar * op
	int GetX();
	int GetY();
private:
	int x;
	int y;
};