#pragma once
#include "Vector2.h"
#include <vector>
#include <string>

class Lines {
public:
	class Line {
	public:
		enum TypeL { Bezier, CatmullRom };
		Line();
		Line(std::string type);
		~Line();
		void AddPoint(Vector2 point);
		void RemovePoint(Vector2 point);
		void RemovePoint();
		bool Full();
		bool Empty();
		int HasThree();
		bool HasPoint(Vector2 point);
		bool operator==(Line other);
		std::string GetType();
		Vector2 GetPoint(int point);
		int r, g, b;
	private:
		Vector2 *P0, *P1, *P2, *P3;
		//int id;
		TypeL lineType;
	};
public:
	Lines();
	void AddLine(Line line);
	void RemoveLine(Line line);
	void ClearLines();
	std::vector<Line> GetListOfLines();
private:
	std::vector<Line> lines;
};