#include <iostream>
#include "SDL_gpu.h"
#include "NFont_gpu.h"
#include "Vector2.h"
#include <functional>
#include "Lines.h"
using namespace std;

Vector2 CubicBezier(Vector2 P0, Vector2 P1, Vector2 P2, Vector2 P3, float t, SDL_Color color) {
																						  //P(t) =
	Vector2 line1 = -(1 * (pow(t, 3) * P0)) + (3 * (pow(t, 2) * P0)) - (3 * t * P0) + P0; // -1[P0]t^3 + 3[P0]t^2 - 3[P0]t + [P0] +
	Vector2 line2 = (3 * (pow(t, 3) * P1)) - (6 * (pow(t, 2) * P1)) + (3 * t * P1);       //  3[P1]t^3 - 6[P1]t^2 + 3[P1]t        +
	Vector2 line3 = -(3 * (pow(t, 3) * P2)) + (3 * (pow(t, 2) * P2));                     // -3[P2]t^3 + 3[P2]t^2                 +
	Vector2 line4 = pow(t, 3) * P3;                                                       //  1[P3]t^3
	return line1 + line2 + line3 + line4;
}

Vector2 CatmullRom(Vector2 P0, Vector2 P1, Vector2 P2, Vector2 P3, float t, SDL_Color color) {
																		    //q(t) = 0.5*( 
	Vector2 lineOne = 2 * P1;                                               // (2*[P1])                                      +
	Vector2 lineTwo = t * (-P0 + P2);                                       // (-[P0] + [P2]) * t                            +
	Vector2 lineThree = (pow(t, 2)) * ((2 * P0) - (5 * P1) + (4 * P2) - P3);// (2 * [P0] - 5 * [P1] + 4 * [P2] - [P3]) * t^2 +
	Vector2 lineFour = (pow(t, 3)) * (-P0 + (3 * P1) - (3 * P2) + P3);      // (-P0 + 3*P1 - 3*P2 + P3) * t^3 )
	return(0.5 * (lineOne + lineTwo + lineThree + lineFour));
}

void DrawSpecial(GPU_Target* screen, Vector2 P0, Vector2 P1, Vector2 P2, Vector2 P3, function<Vector2(Vector2,Vector2,Vector2,Vector2,float,SDL_Color)> callBack, SDL_Color color) {
	Vector2* previousPoint = nullptr;
	Vector2* currentPoint = nullptr;
	for (float t = 0; t < 1; t += (1.0 / 20.0)) {
		if (currentPoint == nullptr)
			currentPoint = new Vector2();
		*currentPoint = callBack(P0, P1, P2, P3, t, color);

		if (previousPoint != nullptr) {
			GPU_Line(screen, previousPoint->GetX(), previousPoint->GetY(), currentPoint->GetX(), currentPoint->GetY(), color);
		}
		else
			previousPoint = new Vector2();
		*previousPoint = *currentPoint;
	}
}

void DrawCubicBezier(GPU_Target* screen, Vector2 P0, Vector2 P1, Vector2 P2, Vector2 P3, SDL_Color color) { DrawSpecial(screen, P0, P1, P2, P3, CubicBezier, color); }

void DrawCatmullRom(GPU_Target* screen, Vector2 P0, Vector2 P1, Vector2 P2, Vector2 P3, SDL_Color color) { DrawSpecial(screen, P0, P1, P2, P3, CatmullRom, color); }

void DrawPoint(GPU_Target* screen, Vector2 point, SDL_Color color) {
	GPU_Circle(screen, point.GetX(), point.GetY(), 2, color);
}

int main(int argc, char* argv[])
{
	GPU_Target* screen = GPU_Init(800, 600, GPU_DEFAULT_INIT_FLAGS);
	if (screen == nullptr)
		return 1;
	SDL_SetWindowTitle(SDL_GetWindowFromID(screen->context->windowID), "My Awesome Game");
	
	GPU_Image* smileImage = GPU_LoadImage("smile.png");
	if (smileImage == nullptr)
		return 2;

	NFont font;
	font.load("FreeSans.ttf", 14);

	const Uint8* keystates = SDL_GetKeyboardState(nullptr);

	float x = 300;
	float y = 300;
	int score = 0;

	int mx = 0, my = 0;

	SDL_Event event;
	bool done = false;
	Lines::Line* currentLine = nullptr;
	Lines allLines;
	string currentLineType = "Bezier";

	while (!done)
	{
		SDL_GetMouseState(&mx, &my);
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				done = true;
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
					done = true;
				else if (event.key.keysym.sym == SDLK_SPACE) {
					if (currentLineType == "Bezier")
						currentLineType = "CatmullRom";
					else if (currentLineType == "CatmullRom")
						currentLineType = "Bezier";
				}
				else if(event.key.keysym.sym == SDLK_w || event.key.keysym.sym == SDLK_s || event.key.keysym.sym == SDLK_a || event.key.keysym.sym == SDLK_d){}
				else {
					allLines.ClearLines();
				}
			}
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				if (event.button.button == SDL_BUTTON_LEFT) {
					if (currentLine == nullptr) {
						currentLine = new Lines::Line(currentLineType);
					}
					currentLine->AddPoint(Vector2(mx, my));
					if (currentLine->Full()) {
						allLines.AddLine(*currentLine);
						currentLine = nullptr;
					}
				}
				if (event.button.button == SDL_BUTTON_RIGHT) {
					if (currentLine != nullptr) {
						currentLine->RemovePoint();
						if (currentLine->Empty())
							currentLine = nullptr;
					}
					else {
						for (int i = 0; i < allLines.GetListOfLines().size(); i++) {
							Lines::Line temp = allLines.GetListOfLines()[i];
							if (temp.HasPoint(Vector2(mx, my))) {
								allLines.RemoveLine(temp);
								temp.RemovePoint(Vector2(mx, my));
								currentLine = &temp;
								break;
							}
						}
					}
				}
			}
		}

		if (keystates[SDL_SCANCODE_A] || keystates[SDL_SCANCODE_LEFT])
			screen->camera.x -= 2.0f;
		else if (keystates[SDL_SCANCODE_D] || keystates[SDL_SCANCODE_RIGHT])
			screen->camera.x += 2.0f;
		if (keystates[SDL_SCANCODE_W] || keystates[SDL_SCANCODE_UP])
			screen->camera.y -= 2.0f;
		else if (keystates[SDL_SCANCODE_S] || keystates[SDL_SCANCODE_DOWN])
			screen->camera.y += 2.0f;

		GPU_ClearRGB(screen, 255, 255, 255); //drawing backgroun

		if (currentLine != nullptr) {
			int hasThreeNum = currentLine->HasThree();
			if (hasThreeNum != -1) {
				SDL_Color color = GPU_MakeColor(currentLine->r, currentLine->g, currentLine->b, 255);
				if (currentLine->GetType() == "Bezier") {
					switch (hasThreeNum) {
					case 0:
						DrawCubicBezier(screen, Vector2(mx, my), currentLine->GetPoint(1), currentLine->GetPoint(2), currentLine->GetPoint(3), color);
						break;
					case 1:
						DrawCubicBezier(screen, currentLine->GetPoint(0), Vector2(mx, my), currentLine->GetPoint(2), currentLine->GetPoint(3), color);
						break;
					case 2:
						DrawCubicBezier(screen, currentLine->GetPoint(0), currentLine->GetPoint(1), Vector2(mx, my), currentLine->GetPoint(3), color);
						break;
					case 3:
						DrawCubicBezier(screen, currentLine->GetPoint(0), currentLine->GetPoint(1), currentLine->GetPoint(2), Vector2(mx, my), color);
						break;
					}
				}
				else {
					switch (hasThreeNum) {
					case 0:
						DrawCatmullRom(screen, Vector2(mx, my), currentLine->GetPoint(1), currentLine->GetPoint(2), currentLine->GetPoint(3), color);
						break;
					case 1:
						DrawCatmullRom(screen, currentLine->GetPoint(0), Vector2(mx, my), currentLine->GetPoint(2), currentLine->GetPoint(3), color);
						break;
					case 2:
						DrawCatmullRom(screen, currentLine->GetPoint(0), currentLine->GetPoint(1), Vector2(mx, my), currentLine->GetPoint(3), color);
						break;
					case 3:
						DrawCatmullRom(screen, currentLine->GetPoint(0), currentLine->GetPoint(1), currentLine->GetPoint(2), Vector2(mx, my), color);
						break;
					}
				}

			}
		}

		for (int i = 0; i < allLines.GetListOfLines().size(); i++) {
			Lines::Line temp = allLines.GetListOfLines().at(i);
			if (temp.Full()) {
				SDL_Color color = GPU_MakeColor(temp.r, temp.g, temp.b, 255);
				for (int i = 0; i < 4; i++)
					DrawPoint(screen, temp.GetPoint(i), color);
				if (temp.GetType() == "Bezier")
					DrawCubicBezier(screen, temp.GetPoint(0), temp.GetPoint(1), temp.GetPoint(2), temp.GetPoint(3), color);
				else
					DrawCatmullRom(screen, temp.GetPoint(0), temp.GetPoint(1), temp.GetPoint(2), temp.GetPoint(3), color);
			}
		}

		font.draw(screen, screen->w - 50, 10, NFont::AlignEnum::RIGHT, "Current Line Type: %s", currentLineType.c_str());

		GPU_Flip(screen); //rendering image

		SDL_Delay(1); 
	}

	GPU_FreeImage(smileImage);
	font.free();

	GPU_Quit();

	return 0;
}