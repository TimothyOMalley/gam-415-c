#include "Lines.h"

Lines::Line::Line() {
	lineType = TypeL::Bezier;
	P0, P1, P2, P3 = nullptr;
	r = rand() % 255;
	g = rand() % 255;
	b = rand() % 255;
}

Lines::Line::Line(std::string type) {
	if (type == "Bezier")
		lineType = Bezier;
	else
		lineType = CatmullRom;
	P0, P1, P2, P3 = nullptr;
	r = rand() % 255;
	g = rand() % 255;
	b = rand() % 255;
}

Lines::Line::~Line() {
	P0, P1, P2, P3 = nullptr;
}

void Lines::Line::AddPoint(Vector2 point) {
	if (P0 == nullptr)
		P0 = new Vector2(point.GetX(), point.GetY());
	else if (P1 == nullptr)
		P1 = new Vector2(point.GetX(), point.GetY());
	else if (P2 == nullptr)
		P2 = new Vector2(point.GetX(), point.GetY());
	else if (P3 == nullptr)
		P3 = new Vector2(point.GetX(), point.GetY());
}

void Lines::Line::RemovePoint(Vector2 point) {
	if (P0 != nullptr) {
		if (point == *P0)
			P0 = nullptr;
	}
	else if (P1 != nullptr) {
		if (point == *P1)
			P1 = nullptr;
	}
	else if (P2 != nullptr) {
		if (point == *P2)
			P2 = nullptr;
	}
	else if(P3 != nullptr)
		if (point == *P3)
			P3 = nullptr;
}

void Lines::Line::RemovePoint() { //removes the last point placed
	if (P3 == nullptr)
		if (P2 != nullptr)
			return RemovePoint(Vector2(P2->GetX(),P2->GetY()));
	if (P2 == nullptr)
		if (P1 != nullptr)
			return RemovePoint(Vector2(P1->GetX(),P1->GetY()));
	if (P1 == nullptr)
		if (P0 != nullptr)
			return RemovePoint(Vector2(P0->GetX(),P0->GetY()));
}

bool Lines::Line::Full() {
	if (P0 != nullptr)
		if (P1 != nullptr)
			if (P2 != nullptr)
				if (P3 != nullptr)
					return true;
	return false;
}

bool Lines::Line::Empty() {
	if (P0 == nullptr)
		if (P1 == nullptr)
			if (P2 == nullptr)
				if (P3 == nullptr)
					return true;
	return false;
}

int Lines::Line::HasThree() {
	if (Full())
		return -1;
	int count = 0;
	if (P0 != nullptr)
		count++;
	if (P1 != nullptr)
		count++;
	if (P2 != nullptr)
		count++;
	if (P3 != nullptr)
		count++;

	if (count == 3) {
		if (P0 == nullptr)
			return 0;
		if (P1 == nullptr)
			return 1;
		if (P2 == nullptr)
			return 2;
		if (P3 == nullptr)
			return 3;
	}

	return -1;
}

bool Lines::Line::HasPoint(Vector2 point) {
	if (*P0 == point)
		return true;
	if (*P1 == point)
		return true;
	if (*P2 == point)
		return true;
	if (*P3 == point)
		return true;
	return false;
}

std::string Lines::Line::GetType() {
	switch (lineType) {
	case Lines::Line::TypeL::Bezier:
		return "Bezier";
	default:
		return "TheOtherOne";
	}
}

Vector2 Lines::Line::GetPoint(int point) {
	switch (point) {
	case 0:
		return *P0;
	case 1:
		return *P1;
	case 2:
		return *P2;
	case 3:
		return *P3;
	}
}

bool Lines::Line::operator==(Line other) {
	if (*P0 == *other.P0)
		if (*P1 == *other.P1)
			if (*P2 == *other.P2)
				if (*P3 == *other.P3)
					return true;
	return false;
}

Lines::Lines() {
	lines = std::vector<Line>();
}

void Lines::AddLine(Lines::Line line) {
	lines.push_back(line);
}

void Lines::RemoveLine(Lines::Line line) {
	auto lineToDelete = std::find(lines.begin(), lines.end(), line);
	lines.erase(lineToDelete);
}

void Lines::ClearLines() {
	lines.clear();
}

std::vector<Lines::Line> Lines::GetListOfLines(){
	return lines;
}

