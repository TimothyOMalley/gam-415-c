#include <iostream>
#include "SDL_gpu.h"
#include "NFont_gpu.h"
#include "GameState.h"
#include "Testing.h"
using namespace std;




int main(int argc, char* argv[])
{
	GPU_Target* screen = GPU_Init(1024, 768, GPU_DEFAULT_INIT_FLAGS);
	if (screen == nullptr)
		return 1;

	SDL_SetWindowTitle(SDL_GetWindowFromID(screen->context->windowID), "My Awesome Game");

	GameState game;

	float dt = 0.0f;
	Uint32 frameStart, frameEnd;
	const Uint8* keystates = SDL_GetKeyboardState(nullptr);
	SDL_Event event;

	while (!game.done)
	{
		frameStart = SDL_GetTicks();
		while (SDL_PollEvent(&event))
		{
			game.HandleEvent(event);
		}

		game.HandleInputState(keystates, dt);

		game.PhysicsUpdate(dt, screen);
		
		game.Render(screen);
		GPU_Flip(screen);

		SDL_Delay(1);
		frameEnd = SDL_GetTicks();
		dt = (frameEnd - frameStart) / 1000.0f;

	}

	GPU_Quit();

	return 0;
}