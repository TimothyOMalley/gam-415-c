#include "PolygonRenderer.h"
using namespace std;

PolygonRenderer::PolygonRenderer(shared_ptr<Transform> newTransform, shared_ptr<VertexHolder> newVertices)
	: transform(newTransform), vertices(newVertices)
{
	color = GPU_MakeColor(255, 0, 0, 255);
}

void PolygonRenderer::Draw(GPU_Target* target, float mass)
{
	auto points = this->vertices->Get();
	if (points.size() < 2)
		return;

	GPU_PushMatrix();
	GPU_Translate(transform->position.x, transform->position.y, 0.0f);
	GPU_Rotate(transform->angleDegrees, 0.0f, 0.0f, 1.0f);

	// TODO: Loop through all points and draw lines between them
	for (auto i = 0; i < points.size()-1; i++)
		for (float j = 0.9f; j < mass; j += 0.05) {
			GPU_Line(target, points[i].x/j, points[i].y/j, points[i + 1].x/j, points[i + 1].y/j, color);
		}
	for (float j = 0.9f; j < mass; j += 0.05)
		GPU_Line(target, points[0].x/j, points[0].y/j, points[points.size()-1].x/j, points[points.size()-1].y/j, color);


	GPU_PopMatrix();
}
