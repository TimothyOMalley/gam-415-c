#pragma once

#include "Entity.h"
#include "SDL.h"

class GameState
{
public:

	enum class CollisionModeEnum { None, Displace, DisplaceAndVelocity };
	CollisionModeEnum collisionMode = CollisionModeEnum::DisplaceAndVelocity;

	std::vector<Entity> entities;

	float moveSpeed;
	bool done;
	bool expPhy;
	
	bool gravity;
	bool editMode = true;

	Entity* selected = nullptr;
	Vector2* selectedVertex = nullptr;

	GameState();

	void HandleEvent(const SDL_Event& event);

	void HandleInputState(const Uint8* keystates, float dt);

	void PhysicsUpdate(float dt, GPU_Target* screen);

	void Render(GPU_Target* screen);

	void AddRandomPolygon();

	Entity* PointInPolyTest();

	bool OppositeSidesOfLine(Vector2 v0, Vector2 v1, int mx, int my);

	void LoadFile();
	
	void WriteToFile();
};