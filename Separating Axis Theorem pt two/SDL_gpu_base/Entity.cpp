#include "Entity.h"
using namespace std;


Entity::Entity(std::vector<Vector2> vertices)
	: transform(make_shared<Transform>())
	, vertexHolder(make_shared<VertexHolder>(vertices))
	, renderer(make_shared<PolygonRenderer>(transform, vertexHolder))
	, rigidbody(make_shared<Rigidbody>())
	, collider(make_shared<PolygonCollider>(transform, vertexHolder))
{}

void Entity::ChangeVelocity(Vector2 change) {
	if (ignoreGravity) {
		SetVelocity(change);
	}
	else {
		rigidbody->velocity += change;
	}
}

void Entity::SetVelocity(Vector2 set) {
	if (ignoreGravity) {
		rigidbody->velocity = Vector2(0,0);
	}
	else {
		rigidbody->velocity = set;
	}
}