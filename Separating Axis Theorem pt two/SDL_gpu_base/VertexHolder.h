#pragma once

#include <vector>
#include <memory>
#include "Vector2.h"
#include "Transform.h"

class VertexHolder
{
private:
	std::vector<Vector2> vertices;

public:
	VertexHolder(std::vector<Vector2> newVertices);

	std::vector<Vector2>& Get();
	const std::vector<Vector2>& Get() const;
	void Set(std::vector<Vector2> newVertices);

	std::vector<Vector2> GetTransformed(std::shared_ptr<Transform> transform);
};