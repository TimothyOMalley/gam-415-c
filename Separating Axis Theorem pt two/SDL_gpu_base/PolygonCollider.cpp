#include "PolygonCollider.h"
using namespace std;



SATProjection::SATProjection()
	: min(numeric_limits<float>::max()), max(-numeric_limits<float>::max())
{}

// Careful: Assumes same axis of projection
bool SATProjection::GetOverlap(const SATProjection& projA, const SATProjection& projB, float& outOverlap)
{
	// TODO: If there is an overlap between the two ranges in projA and projB,
	// there are two possible directions to push object A out of overlap.
	// The smaller amount should be stored in outOverlap and true should be returned.
	if (std::fminf(projA.max, projB.max) > std::fmaxf(projA.min, projB.min)) {
		if (abs(projB.min - projA.max) < abs(projB.max - projA.min))
			outOverlap = projB.min - projA.max;
		else
			outOverlap = projB.max - projA.min;
		//outOverlap = std::fminf(abs((projB.min-projA.max)),abs((projB.max-projA.min)));
		return true;
	}
	
	return false;
}





PolygonCollider::PolygonCollider(shared_ptr<Transform> newTransform, shared_ptr<VertexHolder> newVertices)
	: transform(newTransform), vertices(newVertices)
{}

vector<Vector2> PolygonCollider::GetNormals(const vector<Vector2>& vertices)
{
	vector<Vector2> result;

	// TODO: Calculate one normal per edge (vertex pair) in a loop.
	// The normal of a vector can be calculated as (-y, x).  Normalize it and store in result.
	for (int i = 0; i < vertices.size() - 1; i++)
		result.push_back(Vector2(-(vertices[i+1].y - vertices[i].y), vertices[i+1].x - vertices[i].x).Normalized());
	result.push_back(Vector2(-(vertices[0].y - vertices[vertices.size() - 1].y), vertices[0].x - vertices[vertices.size() - 1].x).Normalized());
	return result;
}

SATProjection PolygonCollider::GetProjection(const vector<Vector2>& vertices, Vector2 axis)
{
	SATProjection result;
	result.axis = axis;

	// TODO: Loop through each vertex.  Calculate the dot product with the axis under consideration.
	// Set result.min to the lowest of these values.  Set result.max to the highest.
	for (Vector2 vertex : vertices) {
		float dotProd = vertex.Dot(axis);
		if (dotProd > result.max)
			result.max = dotProd;
		if (dotProd < result.min)
			result.min = dotProd;
	}
	return result;
}

// Applies the Separating Axis Theorem to find the Minimum Translation Vector (MTV)
bool PolygonCollider::SATGetMTV(shared_ptr<PolygonCollider> A, shared_ptr<PolygonCollider> B, Vector2& outMTV)
{
	// SAT:
	// Calculate normals for both shapes (-y, x) ,/
	// Project all vertices to the normal and get min/max for bounds (dot(vert, normal))
	// Check for overlap (Amin > Bmin && Amin < Bmax)
	// Calculate minimum translation vector (normal * smallestOverlap)

	auto verticesA = A->vertices->GetTransformed(A->transform);
	auto verticesB = B->vertices->GetTransformed(B->transform);

	auto normals = GetNormals(verticesA);
	auto normalsB = GetNormals(verticesB);

	normals.insert(normals.end(), normalsB.begin(), normalsB.end());

	// TODO: For each normal (potential separating axis),
	// get the projection of each polygon onto that axis (using SATProjection and GetProjection()).
	// If there is an overlap between these two projections (SATProjection::GetOverlap()),
	// then keep track of the smallest overlap so far.
	// If there is no overlap, this is a separating axis and you can immediately return false.
	// Once all potential separating axes are checked, store the MTV like so:
	// outMTV = minOverlapAxis * minOverlap;
	// and then return true.
	float smallestOverlap = NULL;
	Vector2 minOverlapAxis;
	for (auto normal : normals) {
		SATProjection projA = GetProjection(verticesA, normal);
		SATProjection projB = GetProjection(verticesB, normal);

		float outOverlap;
		if (SATProjection::GetOverlap(projA, projB, outOverlap)) {
			if (smallestOverlap == NULL) {
				smallestOverlap = outOverlap;
				minOverlapAxis = normal;
			}
			else if (abs(outOverlap) < abs(smallestOverlap)) {
				smallestOverlap = outOverlap;
				minOverlapAxis = normal;
			}
		}else
			return false;
	}
	outMTV = minOverlapAxis * smallestOverlap;
	return true;
}