#include "GameState.h"
#include "SDL_gpu.h"
#include "Entity.h"
#include "Random.h"
#include <map>
#include <iostream>
#include <fstream>
#include <string>


void DrawArrow(GPU_Target* target, float x1, float y1, float x2, float y2, SDL_Color color)
{
	GPU_Line(target, x1, y1, x2, y2, color);

	// Arrow head
	Vector2 a = { x1, y1 };
	Vector2 b = { x2, y2 };
	float dist = Vector2::Distance(a, b);
	if (dist > 1.0f)
	{
		Vector2 sep = b - a;
		sep.Normalize();
		float length = 15;
		if (dist < 15)
			length = dist * (dist / 15);

		float c[3] = { sep.x, sep.y, 0.0f };
		float mat[16];
		GPU_MatrixIdentity(mat);
		GPU_MatrixRotate(mat, 15.0f, 0, 0, 1);
		GPU_VectorApplyMatrix(c, mat);
		GPU_Line(target, x2 - length * c[0], y2 - length * c[1], x2, y2, color);

		c[0] = sep.x;
		c[1] = sep.y;
		c[2] = 0.0f;
		GPU_MatrixIdentity(mat);
		GPU_MatrixRotate(mat, -15.0f, 0, 0, 1);
		GPU_VectorApplyMatrix(c, mat);
		GPU_Line(target, x2 - length * c[0], y2 - length * c[1], x2, y2, color);
	}
}

void ClampToScreen(Entity& e, GPU_Target* screen)
{
	if (e.transform->position.x < 0)
	{
		e.transform->position.x = 0;
		e.rigidbody->velocity.x = -(e.rigidbody->velocity.x/e.rigidbody->mass);
	}
	else if (e.transform->position.x > screen->w)
	{
		e.transform->position.x = screen->w;
		e.rigidbody->velocity.x = -(e.rigidbody->velocity.x/e.rigidbody->mass);
	}

	if (e.transform->position.y < 0)
	{
		e.transform->position.y = 0;
		e.rigidbody->velocity.y = -(e.rigidbody->velocity.y/e.rigidbody->mass);
	}
	else if (e.transform->position.y > screen->h)
	{
		e.transform->position.y = screen->h;
		//e.rigidbody->velocity.y = -(e.rigidbody->velocity.y/e.rigidbody->mass);
		e.rigidbody->velocity.y = 0;
	}
}

void GameState::LoadFile() {
	std::string line;
	std::ifstream fileToLoad("game.txt");
	if (fileToLoad.is_open()) {
		while (getline(fileToLoad, line)) {
			Entity* e = nullptr;
			bool vertDone = false;
			bool shapeDone = false;
			bool posDone = false;
			int atribute = 0;
			int colorAtr = 0;
			int r, g, b, gamma;
			std::vector<Vector2> verticies;
			Vector2 currentPoint = Vector2();
			std::string val = "";
			for (int i = 0; i < line.length(); i++) {
				if (line[i] == ',') {
					if (!shapeDone) {
						if (!vertDone) {
							currentPoint.x = std::stof(val);
							vertDone = true;
						}
						else {
							currentPoint.y = std::stof(val);
							verticies.push_back(currentPoint);
							currentPoint = Vector2();
							vertDone = false;
						}
					}
					else {
						switch (atribute) {
						case 0: //position
							if (!posDone) {
								e->transform->position.x = std::stof(val);
								val = "";
								posDone = true;
							}
							else {
								e->transform->position.y = std::stof(val);
								atribute++;
								val = "";
							}
							break;
						case 1: //rotation
							e->transform->angleDegrees = std::stof(val);
							atribute++;
							val = "";
							break;
						case 2: //color
							switch (colorAtr) {
							case 0: //r
								r = std::stoi(val);
								colorAtr++;
								break;
							case 1: //g
								g = std::stoi(val);
								colorAtr++;
								break;
							case 2: //b
								b = std::stoi(val);
								colorAtr++;
								break;
							case 3: //gama
								gamma = std::stoi(val);
								colorAtr++;
								e->renderer->color = GPU_MakeColor(r, g, b, gamma);
								atribute++;
								break;
							}
							break;
						case 3:
							e->rigidbody->mass = std::stof(val);
							atribute++;
							break;
						case 4:
							if (val == "1") {
								e->ignoreGravity = true;
							}
							else {
								e->ignoreGravity = false;
							}
							atribute++;
							break;
						case 5:
							if (val == "1")
								e->player = true;
							else
								e->player = false;
							entities.push_back(*e);
							e = nullptr;
							break;
						}
					}
					val = "";
				}
				else if (line[i] == 'd') {
					shapeDone = true;
					val = "";
					e = new Entity(verticies);
				}
				else {
					val += line[i];
				}
			}
		}
		fileToLoad.close();
	}
}

void GameState::WriteToFile() {
	std::ofstream fileToWriteTo("game.txt");
	if (fileToWriteTo.is_open()) {
		for (int i = 0; i < entities.size(); i++) {
			std::string line = "";
			for (Vector2 vert : entities[i].vertexHolder->Get()) {
				line += std::to_string(vert.x);
				line += ",";
				line += std::to_string(vert.y);
				line += ",";
			}
			line += "d";
			line += std::to_string(entities[i].transform->position.x);
			line += ",";
			line += std::to_string(entities[i].transform->position.y);
			line += ",";
			line += std::to_string(entities[i].transform->angleDegrees);
			line += ",";
			line += std::to_string(entities[i].renderer->color.r);
			line += ",";
			line += std::to_string(entities[i].renderer->color.g);
			line += ",";
			line += std::to_string(entities[i].renderer->color.b);
			line += ",";
			line += std::to_string(entities[i].renderer->color.a);
			line += ",";
			line += std::to_string(entities[i].rigidbody->mass);
			line += ",";
			line += std::to_string(entities[i].ignoreGravity);
			line += ",";
			line += std::to_string(entities[i].player);
			line += ",";
			fileToWriteTo << line << std::endl;
		}
	}
}



GameState::GameState()
	: collisionMode(CollisionModeEnum::DisplaceAndVelocity),
	entities({})
{
	LoadFile();
	if (!editMode) {
		for (int i = 0; i < entities.size(); i++) {
			if (entities[i].player)
				selected = &entities[i];
		}
	}
	moveSpeed = 300.0f;
	done = false;
	gravity = true;
	expPhy = true;
}

void GameState::HandleEvent(const SDL_Event& event)
{
	if (editMode) {
		if (event.type == SDL_QUIT) {
			if (editMode)
				WriteToFile();
			done = true;
		}
		if (event.type == SDL_KEYDOWN)
		{
			if (event.key.keysym.sym == SDLK_ESCAPE) {
				if (editMode)
					WriteToFile();
				done = true;
			}
			if (event.key.keysym.sym == SDLK_SPACE)
			{
				int newMode = (int)collisionMode + 1;
				if (newMode > 2)
					newMode = 0;
				collisionMode = (GameState::CollisionModeEnum)newMode;
			}
		}
		if (event.type == SDL_MOUSEBUTTONDOWN) {
			if (event.button.button == SDL_BUTTON_RIGHT)
				AddRandomPolygon();
			if (event.button.button == SDL_BUTTON_LEFT) {
				Entity* a = PointInPolyTest();
				selected = a;
				bool has = false;
				if (selected != nullptr) {
					if (selectedVertex != nullptr) {
						for (Vector2 vert : selected->vertexHolder->Get()) {
							if (vert == *selectedVertex)
								has = true;
						}
						if (!has)
							selectedVertex = nullptr;
					}
				}
				//if (a != nullptr)
					//entities.erase(std::remove_if(entities.begin(), entities.end(), [a](Entity e) {return e == *a; }), entities.end());
			}
		}
	}
	else {
		if (event.type == SDL_QUIT)
			done = true;
	}
}

void GameState::HandleInputState(const Uint8* keystates, float dt)
{
	if (selected != nullptr) {
		if (selected->timer > 0) {
			selected->timer -= dt;
		}
	}
	if (editMode) {
		if (keystates[SDL_SCANCODE_T]) {
			selected->player = true;
		}
		if (keystates[SDL_SCANCODE_G])
			gravity = !gravity;
		if (selected != nullptr) {
			if (keystates[SDL_SCANCODE_P]) {
				selected->ignoreGravity = !selected->ignoreGravity;
			}
			if (keystates[SDL_SCANCODE_LEFTBRACKET]) {
				selected->rigidbody->mass -= 0.05;
				if (selected->rigidbody->mass < 1)
					selected->rigidbody->mass = 1;
			}
			else if (keystates[SDL_SCANCODE_RIGHTBRACKET]) {
				selected->rigidbody->mass += 0.05;
			}
			if (keystates[SDL_SCANCODE_UP]) { //scale up
				for (int i = 0; i < selected->vertexHolder->Get().size(); i++) {
					selected->vertexHolder->Get()[i] /= 1 / 1.015f;
				}
			}
			else if (keystates[SDL_SCANCODE_DOWN]) { // scale down
				for (int i = 0; i < selected->vertexHolder->Get().size(); i++) {
					selected->vertexHolder->Get()[i] /= 1.015f;
				}
			}
			if (keystates[SDL_SCANCODE_F]) {
				Entity* temp = selected;
				entities.erase(std::remove_if(entities.begin(), entities.end(), [temp](Entity e) {return e == *temp; }), entities.end());
				selected = nullptr;
				temp = nullptr;
			}
			if (keystates[SDL_SCANCODE_V]) {
				int mx, my;
				SDL_GetMouseState(&mx, &my);

				if (selectedVertex == nullptr) {
					selected->transform->angleDegrees = 0.0f;
					for (int i = 0; i < selected->vertexHolder->GetTransformed(selected->transform).size(); i++) {
						float dx = selected->vertexHolder->GetTransformed(selected->transform)[i].x - mx;
						float dy = selected->vertexHolder->GetTransformed(selected->transform)[i].y - my;
						float dSquared = (dx * dx + dy * dy);
						if (dSquared <= 25 * 25) {
							selectedVertex = &selected->vertexHolder->Get()[i];
							break;
						}
					}
					selected = nullptr;
				}
				else {
					*selectedVertex = Vector2(mx, my) - selected->transform->position;
					selectedVertex = nullptr;
					selected = nullptr;
				}
			}
			if (keystates[SDL_SCANCODE_EQUALS] && keystates[SDL_SCANCODE_RSHIFT]) { // +
				int mx, my;
				SDL_GetMouseState(&mx, &my);
				selected->vertexHolder->Get().push_back(Vector2(mx, my) - selected->transform->position);
				selected = nullptr;
			}
			if (keystates[SDL_SCANCODE_MINUS]) { // -
				if (selected != nullptr) {
					int mx, my;
					SDL_GetMouseState(&mx, &my);
					for (int i = 0; i < selected->vertexHolder->GetTransformed(selected->transform).size(); i++) {
						float dx = selected->vertexHolder->GetTransformed(selected->transform)[i].x - mx;
						float dy = selected->vertexHolder->GetTransformed(selected->transform)[i].y - my;
						float dSquared = (dx * dx + dy * dy);
						if (dSquared <= 25 * 25) {
							auto c = selected->vertexHolder->Get();
							Vector2 in = c[i];
							c.erase(std::remove_if(c.begin(), c.end(), [in](Vector2 vert) {return in == vert; }), c.end());
							break;
						}
					}
					selected = nullptr;
				}
			}
			if (collisionMode == GameState::CollisionModeEnum::DisplaceAndVelocity)
			{
				if (keystates[SDL_SCANCODE_A])
					selected->rigidbody->velocity.x -= moveSpeed * dt;
				else if (keystates[SDL_SCANCODE_D])
					selected->rigidbody->velocity.x += moveSpeed * dt;
				if (keystates[SDL_SCANCODE_W])
					selected->rigidbody->velocity.y -= moveSpeed * dt;
				else if (keystates[SDL_SCANCODE_S])
					selected->rigidbody->velocity.y += moveSpeed * dt;
			}
			else
			{
				if (keystates[SDL_SCANCODE_A])
					selected->transform->position.x -= moveSpeed * dt;
				else if (keystates[SDL_SCANCODE_D])
					selected->transform->position.x += moveSpeed * dt;
				if (keystates[SDL_SCANCODE_W])
					selected->transform->position.y -= moveSpeed * dt;
				else if (keystates[SDL_SCANCODE_S])
					selected->transform->position.y += moveSpeed * dt;
			}

			if (keystates[SDL_SCANCODE_Q])
				selected->transform->angleDegrees -= 120.0f * dt;
			else if (keystates[SDL_SCANCODE_E])
				selected->transform->angleDegrees += 120.0f * dt;

			if (keystates[SDL_SCANCODE_COMMA])
				selected->transform->angleDegrees -= 120.0f * dt;
			else if (keystates[SDL_SCANCODE_PERIOD])
				selected->transform->angleDegrees += 120.0f * dt;
		}
		else if (keystates[SDL_SCANCODE_RETURN]) {
			expPhy = !expPhy;
			for (int i = 0; i < entities.size(); i++)
				entities[i].experimentalPhysics = expPhy;
		}
	}
	else {
	if (selected != nullptr) {
		if (collisionMode == GameState::CollisionModeEnum::DisplaceAndVelocity)
		{
			if (keystates[SDL_SCANCODE_A])
				selected->rigidbody->velocity.x -= moveSpeed * dt;
			else if (keystates[SDL_SCANCODE_D])
				selected->rigidbody->velocity.x += moveSpeed * dt;
			if (keystates[SDL_SCANCODE_W]) {
				if (selected->timer <= 0) {
					if (selected->numberOfJumps > 0) {
						selected->Jump(moveSpeed, dt);
						selected->timer += 1;
						selected->numberOfJumps--;
					}
				}
			}
			else if (keystates[SDL_SCANCODE_S])
				selected->rigidbody->velocity.y += moveSpeed * dt;
		}
		else
		{
			if (keystates[SDL_SCANCODE_A])
				selected->transform->position.x -= moveSpeed * dt;
			else if (keystates[SDL_SCANCODE_D])
				selected->transform->position.x += moveSpeed * dt;
			if (keystates[SDL_SCANCODE_W])
				selected->transform->position.y -= moveSpeed * dt;
			else if (keystates[SDL_SCANCODE_S])
				selected->transform->position.y += moveSpeed * dt;
		}
	}
	}
}


void GameState::PhysicsUpdate(float dt, GPU_Target* screen)
{
	for (int i = 0; i < entities.size(); i++) {
		entities[i].MTV = Vector2();
		entities[i].gotCollision = false;
		entities[i].collissions.clear();

		if (gravity)
			if(!entities[i].ignoreGravity)
				entities[i].rigidbody->velocity += Vector2(0, 9.8f);
		entities[i].rigidbody->Tick(entities[i].transform->position, dt);

		for (int j = 0; j < entities.size(); j++) {
			if (j != i) {
				Vector2 MTV;
				if (PolygonCollider::SATGetMTV(entities[i].collider, entities[j].collider, MTV)) {
					entities[i].collissions.push_back(Entity::Collision(&entities[j], MTV));
					entities[i].gotCollision = true;
				}
			}
		}
	}

	for (int i = 0; i < entities.size(); i++) {
		entities[i].CalculateMTV();

		if (entities[i].gotCollision) {
			if (!entities[i].ignoreGravity) {
				if (collisionMode == CollisionModeEnum::Displace)
					entities[i].transform->position += entities[i].MTV;
				else if (collisionMode == CollisionModeEnum::DisplaceAndVelocity) {
					entities[i].transform->position += entities[i].MTV;

					for (int j = 0; j < entities[i].collissions.size(); j++) {
						Vector2 temp = entities[i].rigidbody->velocity;
						entities[i].SetVelocity((entities[i].collissions[j].collider->rigidbody->velocity / (entities[i].rigidbody->mass / entities[i].collissions[j].collider->rigidbody->mass)));
						entities[i].collissions[j].collider->SetVelocity((temp / (entities[i].collissions[j].collider->rigidbody->mass / entities[i].rigidbody->mass)));
						if (entities[i].collissions[j].collider->ignoreGravity) {
							entities[i].SetVelocity(Vector2(temp.x, -(temp.y/(entities[i].rigidbody->mass+0.5))));//entities[i].rigidbody->velocity.y));
						}
						Entity& tempEnt = entities[i];
						entities[i].collissions[j].collider->collissions.erase(std::remove_if(entities[i].collissions[j].collider->collissions.begin(), entities[i].collissions[j].collider->collissions.end(), [tempEnt](Entity::Collision e) {return *e.collider == tempEnt; }), entities[i].collissions[j].collider->collissions.end());
					}
				}
			}
		}
		ClampToScreen(entities[i], screen);
	}
}

void GameState::Render(GPU_Target* screen)
{
	GPU_ClearRGB(screen, 0, 0, 0);
	for (int i = 0; i < entities.size(); i++) {
		entities[i].renderer->Draw(screen, entities[i].rigidbody->mass);
			for (int j = 0; j < entities[i].collissions.size(); j++)
				DrawArrow(screen,
					entities[i].transform->position.x,
					entities[i].transform->position.y,
					entities[i].transform->position.x + entities[i].collissions[j].colliderMTV.x*-1,
					entities[i].transform->position.y + entities[i].collissions[j].colliderMTV.y*-1,
					entities[i].collissions[j].collider->renderer->color);
			Vector2 perp = Vector2(-entities[i].MTV.y, entities[i].MTV.x);
			Vector2 startPos = { screen->w / 2.0f, screen->h - 100.0f };
			DrawArrow(screen, startPos.x, startPos.y, startPos.x + entities[i].MTV.x, startPos.y + entities[i].MTV.y, entities[i].renderer->color);
	}
}

void GameState::AddRandomPolygon() {
	static Random rand = Random();
	std::vector<Vector2> verticies;
	int numberOfVert = rand.Integer(3, 6);
	float turnAngle = (360 / numberOfVert);

	//create new shape on cursor pos
	int mx, my;
	SDL_GetMouseState(&mx, &my);

	for (int i = 0; i < numberOfVert; i++) {
		float currentAngle = (turnAngle * (i)*(float)(M_PI)) / 180;
		verticies.push_back(Vector2((sin(currentAngle)*100),(cos(currentAngle)*100)));
	}
	entities.push_back(Entity(verticies));
	entities[entities.size() - 1].renderer->color = GPU_MakeColor(rand.Integer(0,255),rand.Integer(0,255),rand.Integer(0,255),255);
	entities[entities.size() - 1].transform->position = Vector2(mx, my);
	entities[entities.size() - 1].rigidbody->mass = 1;
	selected = &entities[entities.size() - 1];
}

Entity* GameState::PointInPolyTest() {
	int mx, my;
	SDL_GetMouseState(&mx, &my);
	int numberOfCrosses = 0;

	//For all shapes on screen, if vertex[i].x >= mx,
	//Check to see if vertexes are on either side of line
	// 
	//If vertex[i].y < vertex[i+1].y then numberOfCrosses++; else numberOfCrosses--;

	for(int i=0;i<entities.size();i++){
		std::vector<Vector2> vertex = entities[i].vertexHolder->GetTransformed(entities[i].transform);
		for (int j = 0; j < vertex.size(); j++) { //for each vertex of shape entities[i]
			Vector2 v0 = vertex[j];
			Vector2 v1;
			if (j == vertex.size() - 1)
				v1 += vertex[0];
			else 
				v1 += vertex[j + 1]; 
			if(fmax(v1.x,v0.x) >= mx){
				if (OppositeSidesOfLine(v0, v1, mx, my)) {
						if (v0.y - v1.y >= 0)
							numberOfCrosses++;
						else
							numberOfCrosses--;
				}
			}
		}
		if (numberOfCrosses != 0)
			return &entities[i];
	}
	return nullptr; 
}

//if my is between v0.y and v1.y
//

bool GameState::OppositeSidesOfLine(Vector2 v0, Vector2 v1, int mx, int my) {
	float pointToCross;
	if (v0.x == v1.x)
		pointToCross = v0.x;
	else {
		float m = ((v1.y - v0.y) / (v1.x - v0.x));
		float b = v1.y - (v1.x * m);
		pointToCross = (my - b) / m; //x value to cross
	}
	if (mx <= pointToCross) {
		if (v0.y >= my) //zero higher
			if (v1.y <= my) //one lower ; on left
				return true;
		if (v0.y <= my) //zero lower
			if (v1.y >= my) // one higher
				return true;
	}
	return false;
}