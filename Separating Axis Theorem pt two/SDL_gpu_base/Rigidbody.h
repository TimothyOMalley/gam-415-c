#pragma once

#include "Vector2.h"

class Rigidbody
{
public:
	float mass;
	Vector2 velocity;
	Vector2 forceToApply;
	float drag;

	bool isStatic;

	Rigidbody();

	void Tick(Vector2& position, float dt);
};