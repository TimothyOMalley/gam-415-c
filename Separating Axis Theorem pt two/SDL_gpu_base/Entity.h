#pragma once

#include <memory>
#include <vector>
#include <map>

#include "Vector2.h"
#include "Transform.h"
#include "VertexHolder.h"
#include "PolygonRenderer.h"
#include "Rigidbody.h"
#include "PolygonCollider.h"

class Entity
{
public:
	std::shared_ptr<Transform> transform;
	std::shared_ptr<VertexHolder> vertexHolder;
	std::shared_ptr<PolygonRenderer> renderer;
	std::shared_ptr<Rigidbody> rigidbody;
	std::shared_ptr<PolygonCollider> collider;


	Entity(std::vector<Vector2> vertices);

	class Collision {
	public:
		Entity *collider;
		Vector2 colliderMTV;
		Collision(Entity *_collider, Vector2 _colliderMTV) {
			collider = _collider;
			colliderMTV = _colliderMTV;
		}
		~Collision() {
			collider = nullptr;
		}
	};

	std::vector<Collision> collissions;

	bool gotCollision;
	Vector2 MTV;
	bool experimentalPhysics = true;
	bool calculated;
	bool ignoreGravity = false;
	bool player = false;
	float timer = 0.0f;
	int numberOfJumps = 5;

	void ChangeVelocity(Vector2 change);
	void SetVelocity(Vector2 set);

	void Jump(float moveSpeed, float dt) {
			rigidbody->velocity.y -= moveSpeed * dt * 250;
	}

	bool operator==(const Entity other) const {
		if (vertexHolder->Get().size() != other.vertexHolder->Get().size())
			return false;
		for (int i = 0; i < vertexHolder->Get().size(); i++) {
			if (vertexHolder->Get()[i].x+transform->position.x != other.vertexHolder->Get()[i].x+other.transform->position.x)
				return false;
			if (vertexHolder->Get()[i].y + transform->position.y != other.vertexHolder->Get()[i].y + other.transform->position.y)
				return false;
		}
		return true;
	}

	int GetCollissionsPosOf(Entity e) {

	}

	void CalculateMTV(){
		MTV = Vector2();
		if (experimentalPhysics) {
			//combine MTVs, if currently compared MTV is smaller magnitude vector of already combined piece, discard
			for (int i = 0; i < collissions.size(); i++) {
				if (MTV.GetAxis() == Vector2(0, 0)) {
					MTV = collissions[i].colliderMTV;
					continue;
				}
				//get axis of collider's MTV
				Vector2 axis = collissions[i].colliderMTV.GetAxis();
				for (int j = 0; j < i; j++) { //go through previously added vector
					if (j != i) {
						if (axis == collissions[j].colliderMTV.GetAxis()) { //if curr vector axis == previously added vec axis
							if (collissions[i].colliderMTV.Magnitude() > collissions[j].colliderMTV.Magnitude()) { //if the new axis > prev added
								MTV -= collissions[j].colliderMTV; //remove previously added
								//MTV += collissions[i].colliderMTV; //added new vec
							}
						}
					}
				}

				MTV += collissions[i].colliderMTV; //add new vec
			}
		}
		else {
			if(collissions.size() > 0)
				MTV = collissions[0].colliderMTV;
		}
	}

};

