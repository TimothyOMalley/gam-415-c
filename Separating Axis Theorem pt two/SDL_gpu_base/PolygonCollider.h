#pragma once

#include <memory>
#include <vector>
#include "Vector2.h"
#include "Transform.h"
#include "VertexHolder.h"

struct SATProjection
{
	Vector2 axis;
	float min, max;

	SATProjection();

	// Careful: Assumes same axis of projection
	static bool GetOverlap(const SATProjection& projA, const SATProjection& projB, float& outOverlap);
};

class PolygonCollider
{
public:

	std::shared_ptr<Transform> transform;
	std::shared_ptr<VertexHolder> vertices;

	PolygonCollider(std::shared_ptr<Transform> newTransform, std::shared_ptr<VertexHolder> newVertices);

	static std::vector<Vector2> GetNormals(const std::vector<Vector2>& vertices);

	static SATProjection GetProjection(const std::vector<Vector2>& vertices, Vector2 axis);

	// Applies the Separating Axis Theorem to find the Minimum Translation Vector
	static bool SATGetMTV(std::shared_ptr<PolygonCollider> A, std::shared_ptr<PolygonCollider> B, Vector2& outMTV);
};