#pragma once
#include <string>
#include <functional>
class Player {
public:
	Player();
	Player(int _playerId, int _playTime, int _gold, int _XP, int _charType, int _level, int _highestStageBeaten, std::string _banStatus);

	int GetPlayerId();
	int GetPlayTime();
	int GetGold();
	int GetXP();
	int GetCharType();
	int GetLevel();
	int GetHighestStageBeaten();
	std::string GetBanStatus();

	void SetPlayerID( int _playerId);
	void SetPlayTime(int _playTime);
	void SetGold(int _gold);
	void SetXP(int _XP);
	void SetCharType(int _charType);
	void SetLevel(int _level);
	void SetHighestStageBeaten( int _highestStageBeaten);
	void SetBanStatus(std::string _banStatus);
private:
	int playerId, playTime, gold, XP, charType, level, highestStageBeaten;
	std::string banStatus;
};