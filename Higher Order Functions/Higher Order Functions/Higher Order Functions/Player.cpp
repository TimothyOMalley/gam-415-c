#include "Player.h"

Player::Player(){
	playerId = -1;
	playTime = 0;
	gold = 0;
	XP = 0;
	charType = 0;
	level = 0;
	highestStageBeaten = 0;
	banStatus = "";
}

Player::Player(int _playerId, int _playTime, int _gold, int _XP, int _charType, int _level, int _highestStageBeaten, std::string _banStatus) {
	playerId = _playerId;
	playTime = _playTime;
	gold = _gold;
	XP = _XP;
	charType = _charType;
	level = _level;
	highestStageBeaten = _highestStageBeaten;
	banStatus = _banStatus;
}

int Player::GetPlayerId() { return playerId; }
int Player::GetPlayTime() { return playTime; }
int Player::GetGold() { return gold; }
int Player::GetXP() { return XP; }
int Player::GetCharType() { return charType; }
int Player::GetLevel() { return level; }
int Player::GetHighestStageBeaten() { return highestStageBeaten; }
std::string Player::GetBanStatus() { return banStatus; }

void Player::SetPlayerID(int _playerId) {
	playerId = _playerId;
}
void Player::SetPlayTime(int _playTime) {
	playTime = _playTime;
}
void Player::SetGold(int _gold) {
	gold = _gold;
}
void Player::SetXP(int _XP) {
	XP = _XP;
}
void Player::SetCharType(int _charType) {
	charType = _charType;
}
void Player::SetLevel(int _level) {
	level = _level;
}
void Player::SetHighestStageBeaten(int _highestStageBeaten) {
	highestStageBeaten = _highestStageBeaten;
}
void Player::SetBanStatus(std::string _banStatus) {
	banStatus = _banStatus;
}