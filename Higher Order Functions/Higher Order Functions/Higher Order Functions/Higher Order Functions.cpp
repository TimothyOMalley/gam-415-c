#include <iostream>
#include <vector>
#include <functional>
#include <fstream>
#include "Player.h"

using namespace std;

Player SetPlayerParams(Player p, string input) {
	int currentParam = 0;
	string currentValue = "";
	for (int i = 0; i < (int)(input.length()); i++) {
		if (input[i] == ',') {
			switch (currentParam) {
			case 0:
				p.SetPlayerID(stoi(currentValue));
				break;
			case 1:
				p.SetPlayTime(stoi(currentValue));
				break;
			case 2:
				p.SetGold(stoi(currentValue));
				break;
			case 3:
				p.SetXP(stoi(currentValue));
				break;
			case 4:
				p.SetCharType(stoi(currentValue));
				break;
			case 5:
				p.SetLevel(stoi(currentValue));
				break;
			case 6:
				p.SetHighestStageBeaten(stoi(currentValue));
				break;
			case 7:
				p.SetBanStatus(currentValue);
				break;
			default:
				throw;
			}
			currentValue = "";
			currentParam++;
		}
		else {
			currentValue += input[i];
		}
	}
	return p;
}

void PrintListOfPlayersAboveLevel17(vector<Player> players) {
	cout << "\n\nPlayers who are above level 17:";
	for (int i = 0; i < (int)(players.size()); i++) {
		if (players[i].GetLevel() > 17) {
			cout << "\nPlayerID: " << players[i].GetPlayerId() << " , Level: " << players[i].GetLevel();
		}
	}
}

void AveragePlaytimeOfAllPlayersWhoHaveBeatenXStage(vector<Player> players, int stagNum) {
	cout << "\n\nAverage Playtime of Players Who Have Beaten Stage " << stagNum;
	float totalTime = 0;
	int numberOfPlayers = 0;
	for (int i = 0; i < (int)(players.size()); i++) {
		if (players[i].GetHighestStageBeaten() >= stagNum) {
			numberOfPlayers++;
			totalTime += players[i].GetPlayTime();
		}
	}
	cout << "\n" << totalTime / numberOfPlayers;
}

void AmountOfGoldGivenOutIfEachPlayerUnderXIsGivenBonusEqualToYPercOfEXP(vector<Player> players, int underLevel, int percentile) {
	cout << "\n\nAmount of Gold Given Out If Each Player Under Level " << underLevel << " Is Given Gold Equal to " << percentile << " % Of Their Current EXP";
	float totalGold = 0;
	for (int i = 0; i < (int)(players.size()); i++) {
		if (players[i].GetLevel() < underLevel) {
			totalGold += players[i].GetXP() * percentile/100;
		}
	}
	cout << "\n" << totalGold;
}

template <typename T>
int FindIf(vector<T> v, function<bool(T)> f) {
	for (int i = 0; i < v.size(); i++) {
		if (f(v[i])) {
			return v[i];
		}
	}
	throw;
}

template <typename T>
int IndexOf(vector<T> v, function<bool(T)> f) {
	for (int i = 0; i < v.size(); i++) {
		if (f(v[i])) {
			return i;
		}
	}
	throw;
}

template <typename T>
vector<T> Map(vector<T> v, function<int(T)> f) {
	vector<int> temp;
	for (int i = 0; i < v.size(); i++) {
		temp.push_back(f(v[i]));
	}
	return temp;
}

template <typename T>
T ReduceLeft(vector<T> v, int initial, function<T(T, T)> f) {
	T returnVal;
	for (int i = IndexOf(v, [initial](T i) { return(initial == i); }); i < v.size(); i++) {
		returnVal = f(returnVal, v[i]);
	}
	return returnVal;
}

template <typename T>
function<bool(T)> Negate(function<bool(T)> f) {
	return [f](T i) {return !f(i); };
}


int main() {
	ifstream fin("player data.csv");
	string line;
	vector<Player> players;
	getline(fin, line);
	while (!fin.eof()) {
		getline(fin, line);
		Player temp;
		temp = SetPlayerParams(temp, line);
		players.push_back(temp);
	}
	fin.close();

	//Now players has all the player data parsed out
	PrintListOfPlayersAboveLevel17(players);
	AveragePlaytimeOfAllPlayersWhoHaveBeatenXStage(players, 6);
	AmountOfGoldGivenOutIfEachPlayerUnderXIsGivenBonusEqualToYPercOfEXP(players, 3, 25);
}