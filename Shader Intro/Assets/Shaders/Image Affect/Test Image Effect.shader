Shader "Custom/Test Image Effect"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        intensity("Intensity",range(0,4)) = 0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float intensity;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                // just invert the colors
                //col.rgb = 1 - col.rgb;

                float d = distance(float2(0.5, 0.5), i.uv);

                
                
                float val = pow(1 - d, intensity);
                //col.rgb = lerp(1-col.rgb,col.rgb,val);
                col.rgb = lerp(1 - col.rgb, col.rgb, step(val, 0.5));
                
                return col;
            }
            ENDCG
        }
    } 
}
