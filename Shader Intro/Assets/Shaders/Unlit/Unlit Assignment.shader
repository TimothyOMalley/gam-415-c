Shader "Unlit/Unlit Assignment"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        debugTime("Debug Time",float) = 0
        Trasparency("Transparency Modifier",float) = 1
        IntensityMod("Animation Intensity",Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float red;
            float timeVar;
            float debugTime;
            float Trasparency;
            float IntensityMod;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                //fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                //red = 1-cos((_Time[1]*3.14159)/2);
                
                /*
                    if(0.5 > time){
                        timeVar = 1-2*time;
                    }else{
                        timeVar = 2*time-1;
                    }
                */
                //timeVar = lerp((1-2)*debugTime,(2*debugTime)-1,step(debugTime,0.5));
                //timeVar = _Time[];
                //float varA = 7.5625 * timeVar * timeVar;
                //float varB = 7.5625 * (timeVar -= 1.5 / 2.75) * timeVar + 0.75;
                //float varC = 7.5625 * (timeVar -= 2.25 / 2.75) * timeVar + 0.9375;
                //float varD = 7.5625 * (timeVar -= 2.625 / 2.75) * timeVar + 0.984375;
                /*
                    if(1/2.75 > timeVar){
                        return varA;
                    }else if(2/2.75 > timeVar){
                        return varB;
                    }else if(2.5/2.75 > timeVar){
                        return varC;
                    }else{
                        return varD;
                    }
                */
                //float redEaseOutBounce = lerp(lerp(lerp(varD,varC,step(timeVar,2.5/2.75)),varB,step(timeVar,2/2.75)),varA,step(timeVar,1/2.75));
                /*
                    if(0.5 > time){
                        red = 1-redEaseOutBounce/2;
                    }else{
                        red = 1+redEaseOutBounce/2;
                    }
                */
                //red = lerp((1-redEaseOutBounce)/2,(1+redEaseOutBounce)/2,step(debugTime,0.5));
                //const float c5  = (2*3.14159)/4.5;
                /*
                    if(time > 0.999){
                        1
                    }else if(0.001 > time){
                        
                    }else if (0.5 > time){

                    }else{

                    }
                */
                //red = lerp(0,lerp(0,lerp((pow(2,-20*_Time[1]+10)*sin((20*_Time[1]-11.125)*c5))/2+1,-(pow(2,20*_Time[1]-10)*sin((20*_Time[1]-11.125)*c5))/2,step(_Time[1],0.5)),step(_Time[1],0.001)),step(0.999,_Time[1]));
                red = (lerp(1-cos((_Time[1]*3.14159)/2),lerp(4*_Time[1]*_Time[1]*_Time[1],1-pow(-2*_Time[1]+2,3)/2,step(_Time[1],1)),step(_Time[1],0.5)))*IntensityMod;
                float4 col = float4(i.uv.x*red,(1-i.uv.x+i.uv.y)/red,1-i.uv.y*red,Trasparency);
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
