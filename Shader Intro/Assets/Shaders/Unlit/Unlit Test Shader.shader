Shader "Unlit/Unlit Test Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        Red("Red Value", Range(0,1)) = 1
        Blue("Blue Value", Range(0,1)) = 1
        Green("Green Value", Range(0,1)) = 1
        Transparency("Transparent Value", Range(0,1)) = 1
        PrimaryColor("Primary Color", Color) = (1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float Red;
            float Blue;
            float Green;
            float Transparency;
            float3 PrimaryColor;

            v2f vert (appdata v)
            {
                v2f o;
                //o.vertex = UnityObjectToClipPos(v.vertex);
                o.vertex = UnityObjectToClipPos(v.vertex * (1+sin(Blue*_Time[1]))/2);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                // sample the texture
                //fixed4 col = tex2D(_MainTex, i.uv + float2(_Time[1],0.0));
                float4 col = float4(i.uv.x,((i.uv.x+i.uv.y)/2),i.uv.y,1);
                //float4 col = float4(Red, Green, Blue, Transparency);
                //float4 col = float4(PrimaryColor.rgb, 1);
                //float4 col = float4(PrimaryColor.rrrr);

                //col = float4(step(col.r, 0.5), col.gb, 1);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
