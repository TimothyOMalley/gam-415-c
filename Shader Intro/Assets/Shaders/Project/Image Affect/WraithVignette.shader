Shader "Custome/WraithVignette"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _DistortionTex("Distrion Texture", 2D) = "black" {}
        _Cutoff("Cutoff", range(0,3)) = 0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            sampler2D _DistorionTex;
            float _Cutoff;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 value = tex2D(_DistorionTex, i.uv);

                float alpha = step(value.r+value.g+value.b,_Cutoff);
                
                fixed4 col = tex2D(_MainTex, i.uv);

                //col = value;
                
                col.rgb = 1 - col.rgb;
                col.rgb = col.bbb;
                col.r = col.r;
                col.g = col.g;
                col.b = col.b*1.5;
                col.rgb *= value.rgb;
                col *= value*alpha;
                return col;
            }
            ENDCG
        }
    }
}
