Shader "Custom/TimeDistortion"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _PrismEffectIntensity("Prism Effect Intensity", Range(0,1)) = 0
        _DistanceMod("Distance Modifier",Range(0,3)) = 0
        _SaturationIntensity("Saturation Intensity",Range(0,5)) = 0
        _RippleThickness("Ripple Thickness",Range(0,1)) = 0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float _PrismEffectIntensity;
            float _DistanceMod;
            float _SaturationIntensity;
            float _RippleThickness;

            fixed4 frag (v2f i) : SV_Target
            {

                //fixed4 col = tex2D(_MainTex, i.uv);
                // just invert the colors
                //col.rgb = 1 - col.rgb;
                float rip = distance(float2(0.5,0),i.uv);
                //Prism Effect
                float dist = distance(float2(0.5,0.5),i.uv);

                float d = lerp(lerp(0,pow(10*dist,(1-_DistanceMod/3)),step(rip,(_DistanceMod*2))),0,step(rip,(_DistanceMod/2)));

                fixed4 colr = tex2D(_MainTex,i.uv+0.01*_PrismEffectIntensity*(d));
                fixed4 colg = tex2D(_MainTex,i.uv);
                fixed4 colb = tex2D(_MainTex,i.uv-0.01*_PrismEffectIntensity*(d));

                fixed4 col = fixed4(colr.r,colg.g,colb.b,1);

                //Color saturation
                fixed3 saturated = fixed3(col.r,col.g,col.b);
                saturated.r = col.r*((col.r*(_SaturationIntensity))+(1-(_SaturationIntensity)));
                saturated.g = col.g*((col.g*(_SaturationIntensity))+(1-(_SaturationIntensity)));
                saturated.b = col.b*((col.b*(_SaturationIntensity))+(1-(_SaturationIntensity)));

                //Ripple
                
                col.rgb = lerp(lerp(col.rrr,saturated.rgb,step(rip,(_DistanceMod*2))),col.rgb,step(rip,(_DistanceMod/2)));
            
                return col;
            }
            ENDCG
        }
    }
}
/*
* Effect  to implament:
* Based on how far away from the center of the screen, seperate r,g,b values to make a prism effect
*       Blue goes away from camera, green stays, red comes closer?
* Create a single ripple distortion that travels away from the player
* Following that ripple, saturate, then grayscale the screen
*/
