Shader "Custom/EnchantmentGlimmer"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _EnchantColor("Enchantment Color",Color) = (1,1,1,1)
        _EnchantColorOpacity("Enchantment Color Opacity",Range(0,1)) = 0
        _EnchantmentGlimmer("Enchantment Glimmer Texture",2D) = "white" {}
        _Cutoff("Cutoff Glim Tex Darkness",Range(0,3)) = 0
    }
    SubShader
    {
        Tags {"RenderType"="Opaque"}
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        sampler2D _EnchantmentGlimmer;
        fixed4 _EnchantColor;
        float _Cutoff;
        //float _EnchantA;
        float _EnchantColorOpacity;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 enchant = tex2D(_EnchantmentGlimmer,IN.uv_MainTex+(_Time[0]*4));
            enchant.rgb = 1-enchant.rgb;
            
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            
            float _EnchantA = step(enchant.r+enchant.b+enchant.g,_Cutoff);

            enchant.rgb = 1-enchant.rgb;
            enchant.rgb *= ((_EnchantColor*_EnchantColorOpacity)+(1*(1-_EnchantColorOpacity)));

            o.Albedo = c.rgb;
            o.Albedo *= (((enchant.rgb)*_EnchantA)+(1*(1-_EnchantA)));
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
