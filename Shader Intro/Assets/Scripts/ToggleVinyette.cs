using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class ToggleVinyette : MonoBehaviour{
    public Material mat;
    private bool toggle = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (toggle)
            {
                mat.SetFloat("intensity", 0.2f);
            }
            else
            {
                mat.SetFloat("intensity", 2f);
            }
            toggle = !toggle;
        }
    }
}
