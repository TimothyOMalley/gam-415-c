using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UseTimeDistortion : MonoBehaviour {
    public Material mat;
    public Material secondMat;

    private void OnRenderImage(RenderTexture source, RenderTexture destination) {
        if(geomatry) {
            Graphics.Blit(source, destination, secondMat);
        } else {
            Graphics.Blit(source, destination, mat);
        }
    }
    public bool geomatry = false;
    bool running = false;
    float duration = 5f;
    float time = 0f;
    bool pause = false;
    float secondaryTime = 0f;
    float durationTWo = 2.5f;

    private void Update() {
        if(Input.GetMouseButtonDown(0))
            if(!geomatry) {
                geomatry = true;
            } else {
                geomatry = false;
            }

        if(!running)
            if(Input.GetMouseButtonDown(1)) {
                running = true;
            }
        if(running) {
            if(pause) {
                if(secondaryTime >= durationTWo)
                    pause = false;
                secondaryTime += Time.deltaTime;
            }
            if(!pause) {
                time += Time.deltaTime;
                //mat.SetFloat("_DistanceMod", 3 - Mathf.Abs(-duration / 2 + time / 0.75f));
                //3 -> 0 -> 3
                //0 -> dur/2 -> dur
                mat.SetFloat("_DistanceMod", (Mathf.Abs(-0.5f + (time / duration))) * 6); // 0 -> 1 0.5 = 3
                if(time >= duration / 2)
                    if(!pause)
                        pause = true;
                if(time > duration) {
                    time = 0;
                    running = false;
                    secondaryTime = 0;
                    pause = false;
                }

            }
        }

    }
}