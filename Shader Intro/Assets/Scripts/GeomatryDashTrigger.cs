using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeomatryDashTrigger : MonoBehaviour{
    [SerializeField] Color transitionColor = Color.white;
    [SerializeField] float transitionTime = 0;
    float countedTime = 0f;
    Color firstColor;
    Color difference;
    Material mat;
    private void OnTriggerEnter(Collider other) {
        mat = other.GetComponentInChildren<UseTimeDistortion>().secondMat;
    }

    private void OnTriggerStay(Collider other) {
        if(mat != null) {
            if(transitionTime > 0) {
                if(firstColor == null) {
                    countedTime += Time.deltaTime;
                    firstColor = mat.GetColor("_Color");
                    difference = firstColor - transitionColor;
                }
            } else {
                mat.SetColor("_Color", transitionColor);
            }
        }
    }

    private void Update() {
        if(countedTime > 0 && countedTime < transitionTime) {
            if(countedTime < transitionTime)
                countedTime += Time.deltaTime;
            mat.SetColor("_Color", firstColor + (difference * ((countedTime / transitionTime)+(1-(countedTime/transitionTime)))));
        }
    }
}
