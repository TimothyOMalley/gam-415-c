using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AssignmentImageAffect : MonoBehaviour{
    public Material mat;

    private void OnRenderImage(RenderTexture source, RenderTexture destination) {
        //RenderTexture renderTex = RenderTexture.GetTemporary(source.width, source.height);

        //Graphics.Blit(source, renderTex);
       

        Graphics.Blit(source, destination, mat);
        //RenderTexture.ReleaseTemporary(renderTex);
    }

}
