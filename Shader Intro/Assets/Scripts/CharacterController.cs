using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [SerializeField] float speed = 1f;
    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<Rigidbody>().MovePosition(transform.position + (transform.forward * Input.GetAxis("Vertical") * speed * Time.deltaTime));
        gameObject.transform.Rotate(transform.up * Input.GetAxis("Horizontal") * Time.deltaTime *20);
    }
}
